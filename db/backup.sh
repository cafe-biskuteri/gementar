#!/bin/sh

consider() { test -e "$1" && FILES="$FILES $1"; }

FILES="main.properties"
consider "main.script"
consider "main.log"
consider "main.data"
consider "main.backup"
consider "main.lobs"
consider "main.tmp"
consider "object.properties"
consider "object.script"
consider "object.log"
consider "object.data"
consider "object.backup"
consider "object.lobs"
consider "object.tmp"

test -n "$FILES" || exit
OUTFILE="backup-$(date +%d-%M_%S_%H-%m-%4Y).tar.gz"
echo "$FILES" ">" "$OUTFILE"
tar czf "$OUTFILE" $FILES

# This is less safe than HSQLDB's own backup tooling, but it
# has the advantage of multiple databases within one backup file.

