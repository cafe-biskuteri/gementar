
CREATE
TABLE Actors
(id VARCHAR(128),
created TIMESTAMP,
entity VARCHAR(2048));

CREATE
TABLE Objects
(id VARCHAR(128),
created TIMESTAMP,
entity VARCHAR(2048));

CREATE
TABLE Activities
(id VARCHAR(128),
created TIMESTAMP,
entity VARCHAR(2048));


CREATE
TABLE Public
(created TIMESTAMP,
activityId VARCHAR(128));

CREATE
TABLE Inboxes
(actorUsername VARCHAR(128),
created TIMESTAMP,
activityId VARCHAR(128));

CREATE
TABLE Outboxes
(actorUsername VARCHAR(128),
created TIMESTAMP,
activityId VARCHAR(128));


CREATE
TABLE Accounts
(actorUsername VARCHAR(128),
passphraseHash VARCHAR(512),
passphraseSalt VARCHAR(64),
privateKey VARCHAR(2048),
publicKey VARCHAR(2048));

CREATE
TABLE AccessTokens
(actorUsername VARCHAR(128),
token VARCHAR(1024),
expiryDate TIMESTAMP);


CREATE
TABLE JobLog
(id VARCHAR(128),
status VARCHAR(64),
startDate TIMESTAMP,
endDate TIMESTAMP,
summary VARCHAR(256));


CREATE
TABLE ServerInfo
(databaseVersion INT);

INSERT
INTO ServerInfo
VALUES 16;
