
CREATE
TABLE Attachments
(id VARCHAR(128),
created TIMESTAMP,
entity VARCHAR(2048),
data BLOB);


CREATE
TABLE ServerInfo
(databaseVersion INT);

INSERT
INTO ServerInfo
VALUES 16;
