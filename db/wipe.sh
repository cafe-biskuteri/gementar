#!/bin/sh

consider() { test -e "$1" && FILES="$FILES
$1"; }

FILES="main.properties"
consider "main.script"
consider "main.log"
consider "main.data"
consider "main.backup"
consider "main.lobs"
consider "main.tmp"
consider "object.properties"
consider "object.script"
consider "object.log"
consider "object.data"
consider "object.backup"
consider "object.lobs"
consider "object.tmp"

test -n "$FILES" || exit
echo "$FILES"
rm -r -I $FILES
