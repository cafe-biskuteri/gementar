#!/bin/sh

classname() { echo "$1" | sed -E 's|\.(java\|class)$||'; }

LIBS="/usr/share/java/javax.json.jar"
LIBS="$LIBS:/usr/share/java/hsqldb.jar"
CLASSPATH="bin:bin/dbmigra:bin/tests:$LIBS"

if test "$1" = "c"
then
	javac \
		-cp "$CLASSPATH" \
		-d "bin" \
		-Xlint:unchecked \
		src/*.java
	test $? -eq 0 && javac \
		-cp "$CLASSPATH" \
		-d "bin/dbmigra" \
		-Xlint:unchecked \
		src/dbmigra/*.java
fi

if test "$1" = "r"
then
	test $# -eq 1 && set -- r Gemetar
	shift 1
	java -cp "$CLASSPATH" -ea "$@"
fi

if test "$1" = "ct"
then
	javac \
		-cp "$CLASSPATH" \
		-d "bin/tests" \
		src/tests/*.java  # sic. All files in there
fi

if test "$1" = "rt"
then
	CLASSNAMES=""
	cd src/tests
	for SOURCEFILE in *Tests.java
	do
		CLASSNAMES="$(classname "$SOURCEFILE") $CLASSNAMES"
	done

	cd ../..
	for CLASSNAME in $CLASSNAMES
	do
		java \
			-cp "$CLASSPATH" \
			-ea $CLASSNAME || break
	done

	test -e testDataIds.txt && rm testDataIds.txt
fi

if test "$1" = "clean"
then
	cd bin  # For some safety
	files="$(find "." -regex '.*\.class$')"

	printf "%s" "$files"
	test -n "$files" && echo
	printf "%s" "Remove listed files? Y/N: "
	read response
	response="$(echo "$response" | cut -c1)"
	test "$response" = "Y" -o "$response" = "y" || exit

	test -n "$files" && rm $files
fi

if test "$1" = "dbinit"
then
	java \
		-cp "$CLASSPATH" \
		-ea DataAPI script main db/initMain.sql
	java \
		-cp "$CLASSPATH" \
		-ea DataAPI script object db/initObject.sql
fi

if test "$1" = "dbwipe"
then
	cd db
	./wipe.sh
fi

if test "$1" = "dbback"
then
	cd db
	./backup.sh
fi

if test "$1" = "db"
then
	test "$2" = "main" -o "$2" = "m" && db="main"
	test "$2" = "object" -o "$2" = "o" && db="object"
	test "$db" = "main" -o "$db" = "object" || exit

	if test "$3" = "--serv"
	then
		SPEC_PREFIX="hsql://localhost/gemetar_"
		shift 3
	else
		SPEC_PREFIX="file:db/"
		shift 2
	fi

	java \
		-cp "/usr/share/java/hsqldb.jar" \
		org.hsqldb.util.DatabaseManagerSwing \
		-url "jdbc:hsqldb:$SPEC_PREFIX$db" \
		-user "Rani" -password "" \
		"$@"
fi

