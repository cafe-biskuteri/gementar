
import java.sql.SQLException;
import java.util.concurrent.Callable;
import javax.json.JsonObject;

class
DeliverJob implements Callable<Void> {

	DataAPI
	dataAPI;

	NetworkAPI
	networkAPI;

	String
	activityId;

//		-=%=-

	public Void
	call()
	throws SQLException
	{
		JsonObject activity = dataAPI.getActivity(activityId);
		if (activity == null)
			throw new IllegalArgumentException(
				"Activity with that ID not found in database");

		networkAPI.deliverActivity(activity);
		return null;
	}

	public String
	toString()
	{
		return "Deliver: Activity <%<" + activityId + ">%>";
	}

//	-=%=-

	DeliverJob(
		DataAPI dataAPI, NetworkAPI networkAPI,
		String activityId)
	{
		assert dataAPI != null;
		assert networkAPI != null;
		assert activityId != null;

		this.dataAPI = dataAPI;
		this.networkAPI = networkAPI;
		this.activityId = activityId;
	}

}
