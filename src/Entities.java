
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import java.time.ZonedDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;

class
Entities {

	private static final String
	VERSION_ID = "3";

//	---%-@-%---

	public static boolean
	validActor(JsonObject e)
	{
		if (e == null) return false;

		if (e.get("@context") == null) return false;

		if (!(e.get("id") instanceof JsonString)) return false;

		if (!(e.get("username") instanceof JsonString))
			return false;

		return true;
	}

	public static boolean
	validObject(JsonObject e)
	{
		if (e == null) return false;

		if (!(e.get("type") instanceof JsonString)) return false;

		if (!(e.get("content") instanceof JsonString))
			return false;

		return true;
	}

	public static boolean
	validActivity(JsonObject e)
	{
		if (e == null) return false;

		if (e.get("@context") == null) return false;

		if (!(e.get("id") instanceof JsonString)) return false;

		if (!(e.get("type") instanceof JsonString)) return false;

		if (!(e.get("actor") instanceof JsonString)) return false;

		if (!(e.get("object") instanceof JsonString))
			return false;

		if (!(e.get("to") instanceof JsonArray)) return false;

		return true;
	}

	public static boolean
	validAttachment(JsonObject e)
	{
		if (e == null) return false;

		if (!(e.get("id") instanceof JsonString)) return false;

		return true;
	}


	public static JsonObject
	generateActor(JsonObject base)
	{
		assert base != null;
		String username = base.getString("username", null);
		String created = base.getString("created", null);
		if (username == null) return null;

		if (created == null)
			created = ZonedDateTime.now().toString();
		String id = ActivityPubUtil
			.localShortIdToId("actor", username);
		
		return copy(base)
			.add("@context", ActivityPubUtil.basicContext)
			.add("id", id)
			.add("type", "Person")
			.add("name", username)
			.add("created", created)
			.add("creationVersion", VERSION_ID)
			.build();
	}

	public static JsonObject
	generateObject(JsonObject base)
	{
		assert base != null;
		String shortId = base.getString("shortId", null);
		String type = base.getString("type", null);
		String content = base.getString("content", null);
		String created = base.getString("created", null);
		if (shortId == null) return null;
		if (type == null) return null;
		if (content == null) return null;

		if (created == null)
			created = ZonedDateTime.now().toString();
		String id = ActivityPubUtil
			.localShortIdToId("object", shortId);
		
		return copy(base)
			.add("id", id)
			.add("type", type)
			.add("created", created)
			.add("creationVersion", VERSION_ID)
			.build();
	}

	public static JsonObject
	generateActivity(JsonObject base)
	{
		assert base != null;
		String shortId = base.getString("shortId", null);
		String type = base.getString("type", null);
		String actorId = base.getString("actor", null);
		String objectId = base.getString("object", null);
		String created = base.getString("created", null);
		String propagation = base.getString("propagation", null);
		JsonArray targets = getJsonArray(base, "targets", null);
		if (shortId == null) return null;
		if (type == null) return null;
		if (actorId == null) return null;
		if (objectId == null) return null;
		if (propagation == null) return null;
		if (targets == null) return null;
		
		if (created == null)
			created = ZonedDateTime.now().toString();
		String id = ActivityPubUtil
			.localShortIdToId("activity", shortId);

		JsonArrayBuilder to = copy(targets, JsonString.class);
		if (propagation.equals("Public")) to.add("as:Public");

		return copy(base)
			.add("@context", ActivityPubUtil.basicContext)
			.add("id", id)
			.add("created", created)
			.add("creationVersion", VERSION_ID)
			.add("to", to.build())
			.build();
	}

	public static JsonObject
	generateAttachment(JsonObject base)
	{
		assert base != null;
		String shortId = base.getString("shortId", null);
		String created = base.getString("created", null);
		if (shortId == null) return null;

		if (created == null)
			created = ZonedDateTime.now().toString();
		String id = ActivityPubUtil
			.localShortIdToId("attachment", shortId);

		return copy(base)
			.add("id", id)
			.add("created", created)
			.add("creationVersion", VERSION_ID)
			.build();
	}


	public static JsonObject
	filterPrivateFromActor(JsonObject actor)
	{
		Set<String> filtered = new HashSet<>();
		filtered.add("username");
		filtered.add("creationVersion");

		return copy(actor, filtered).build();
	}

	public static JsonObject
	filterPrivateFromObject(JsonObject object)
	{
		Set<String> filtered = new HashSet<>();
		filtered.add("shortId");
		filtered.add("creationVersion");
		filtered.add("bto");
		filtered.add("bcc");

		return copy(object, filtered).build();
	}

	public static JsonObject
	filterPrivateFromActivity(JsonObject activity)
	{
		Set<String> filtered = new HashSet<>();
		filtered.add("shortId");
		filtered.add("creationVersion");
		filtered.add("bto");
		filtered.add("bcc");
		filtered.add("propagation");
		filtered.add("targets");

		return copy(activity, filtered).build();
	}

	public static JsonObject
	filterPrivateFromAttachment(JsonObject attachment)
	{
		Set<String> filtered = new HashSet<>();
		filtered.add("shortId");
		filtered.add("creationVersion");

		return copy(attachment, filtered).build();
	}

//	 -	-%-	 -

	private static JsonArray
	getJsonArray(JsonObject object, String key, JsonArray def)
	{
		JsonValue v = object.get(key);
		if (v instanceof JsonArray) return (JsonArray)v;
		else return def;
	}

	private static JsonObjectBuilder
	copy(JsonObject json, Set<String> without)
	{
		JsonObjectBuilder b = Json.createObjectBuilder();
		for (String key: json.keySet())
			if (!without.contains(key))
				b.add(key, json.get(key));
		return b;
	}

	private static JsonObjectBuilder
	copy(JsonObject json)
	{
		return copy(json, Collections.emptySet());
	}

	private static JsonArrayBuilder
	copy(JsonArray array, Class monotype)
	{
		JsonArrayBuilder b = Json.createArrayBuilder();
		for (JsonValue v: array)
			if (!monotype.isInstance(v)) continue;
			else b.add(v);
		return b;
	}

}
