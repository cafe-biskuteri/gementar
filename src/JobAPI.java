
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;

class
JobAPI {

	DataAPI
	dataAPI;

//	 -	-%-	 -

	ExecutorService
	executor;

//	---%-@-%---

	List<String>
	queueSequence(List<Callable<Void>> jobs, List<String> summaries)
	{
		assert !executor.isShutdown();
		assert !jobs.isEmpty();
		assert jobs.size() == summaries.size();

		CompletableFuture<Void> jobPipe = CompletableFuture
			.completedFuture(null);
		List<String> jobIds = new ArrayList<>();
		for (int o = 0; o < jobs.size(); ++o)
		{
			Callable<Void> job = jobs.get(o);
			String summary = summaries.get(o);

			LoggedJob lJob = new LoggedJob(dataAPI, job, summary);
			jobIds.add(lJob.getId());

			Runnable rJob = new FutureTask<Void>(job);
			jobPipe = jobPipe.thenRunAsync(rJob, executor);
		}

		return jobIds;
	}

	public String
	queue(Callable<Void> job, String summary)
	{
		List<Callable<Void>> jobs = new ArrayList<>(1);
		List<String> summaries = new ArrayList<>(1);
		jobs.add(job);
		summaries.add(summary);

		return queueSequence(jobs, summaries).get(0);
	}

	public void
	shutdown()
	{
		executor.shutdown();
	}

//	---%-@-%---

	JobAPI(DataAPI dataAPI)
	{
		this.dataAPI = dataAPI;

		executor = Executors.newFixedThreadPool(4);
	}

}

class
LoggedJob implements Callable<Void> {

	private DataAPI
	dataAPI;

	private Callable<Void>
	job;

	private String
	summary;

//	 -	-%-	 -

	private String
	id;

//	---%-@-%---

	public Void
	call()
	throws Exception
	{
		recordJobUpdate("STARTED");
		try
		{
			job.call();
		}
		catch (Throwable t)
		{
			recordJobUpdate("FAILED");
			throw t;
		}
		recordJobUpdate("COMPLETED");
		return null;
	}

	public String
	getId() { return id; }

//	 -	-%-	 -

	private void
	recordJobUpdate(String state)
	throws IOException
	{
		try
		{
			if (state.equals("STARTED"))
			{
				dataAPI.recordNewJob(
					id, summary, state, ZonedDateTime.now());
			}
			else
			{
				dataAPI.recordJobUpdate(
					id, state, ZonedDateTime.now());
			}
		}
		catch (SQLException eSql)
		{
			throw new IOException(
				"Cannot log job state in database", eSql);
		}
	}

//	---%-@-%---

	LoggedJob(DataAPI dataAPI, Callable<Void> job, String summary)
	{
		assert dataAPI != null;
		assert job != null;
		if (summary == null) summary = "(no summary specified)";

		this.dataAPI = dataAPI;
		this.job = job;
		this.summary = summary;
		this.id = generateID(job);
	}

//	 -	-%-	 -

	private String
	generateID(Callable<Void> job)
	{
		StringBuilder b = new StringBuilder();
		b.append(ZonedDateTime.now().toEpochSecond());
		b.append(this.hashCode());
		b.append(job.hashCode());
		// Good luck colliding this way..
		return b.toString();
	}

}