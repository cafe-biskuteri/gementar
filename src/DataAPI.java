
import java.util.List;
import java.util.ArrayList;
import java.util.Base64;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonException;
import javax.json.JsonObjectBuilder;
import javax.json.stream.JsonParsingException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.KeyFactory;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.security.spec.KeySpec;
import java.security.spec.InvalidKeySpecException;
import org.hsqldb.server.Server;
import org.hsqldb.server.ServerProperties;

class
DataAPI {

	private Connection
	mainDbConn,
	objectDbConn;

//  -=- --- -%- --- -=-

	private KeyPairGenerator
	keypairGen;

	private MessageDigest
	hasher;

	private KeyFactory
	keyFactory;



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



	public synchronized JsonObject
	getActor(String id)
	throws SQLException
	{
		String jsonr = singleLookup(mainDbConn, "Actors", id);
		return jsonr != null ? toJsonObject(jsonr) : null;
	}

	public synchronized void
	saveActor(JsonObject actor)
	throws SQLException
	{
		String actorId = actor.getString("id", null);
		assert actorId != null;

		Timestamp created = getTimestamp(actor, "created");
		assert created != null;

		mainDbConn.setAutoCommit(false);
		boolean completed = false;
		try
		{
			PreparedStatement s = mainDbConn.prepareStatement(
				"\n INSERT" +
				"\n INTO Actors" +
				"\n (id, created, entity)" +
				"\n VALUES ?, ?, ?;");
			s.setString(1, actorId);
			s.setTimestamp(2, created);
			s.setString(3, actor.toString());
			s.executeUpdate();

			completed = true;
		}
		finally
		{
			if (completed) mainDbConn.commit();
			else mainDbConn.rollback();
			mainDbConn.setAutoCommit(true);
		}
	}

//	 -      -%-      -

	public synchronized JsonObject
	getObject(String id)
	throws SQLException
	{
		String jsonr = singleLookup(mainDbConn, "Objects", id);
		return jsonr != null ? toJsonObject(jsonr) : null;
	}

	public synchronized void
	saveObject(JsonObject object)
	throws SQLException
	{
		String objectId = object.getString("id", null);
		assert objectId != null;

		Timestamp created = getTimestamp(object, "created");
		assert created != null;

		mainDbConn.setAutoCommit(false);
		boolean completed = false;
		try
		{
			PreparedStatement s = mainDbConn.prepareStatement(
				"\n INSERT" +
				"\n INTO Objects" +
				"\n (id, created, entity)" +
				"\n VALUES ?, ?, ?;");
			s.setString(1, objectId);
			s.setTimestamp(2, created);
			s.setString(3, object.toString());
			s.executeUpdate();

			completed = true;
		}
		finally
		{
			if (completed) mainDbConn.commit();
			else mainDbConn.rollback();
			mainDbConn.setAutoCommit(true);
		}
	}

//	 -      -%-      -

	public synchronized JsonObject
	getActivity(String id)
	throws SQLException
	{
		String jsonr = singleLookup(mainDbConn, "Activities", id);
		return jsonr != null ? toJsonObject(jsonr) : null;
	}

	public synchronized List<JsonObject>
	readActivities(
		ZonedDateTime before, ZonedDateTime after, int pageSize)
	throws SQLException
	{
		List<String> jsonrs = pageLookup(
			mainDbConn, "Activities", "entity",
			null, null,
			before, after, pageSize);

		List<JsonObject> entities = new ArrayList<>();
		for (String jsonr: jsonrs)
			entities.add(toJsonObject(jsonr));

		return entities;
	}

	public synchronized void
	saveActivity(JsonObject activity)
	throws SQLException
	{
		String activityId = activity.getString("id", null);
		assert activityId != null;

		Timestamp created = getTimestamp(activity, "created");
		assert created != null;

		mainDbConn.setAutoCommit(false);
		boolean completed = false;
		try
		{
			PreparedStatement s = mainDbConn.prepareStatement(
				"\n INSERT" +
				"\n INTO Activities" +
				"\n (id, created, entity)" +
				"\n VALUES ?, ?, ?;");
			s.setString(1, activityId);
			s.setTimestamp(2, created);
			s.setString(3, activity.toString());
			s.executeUpdate();

			completed = true;
		}
		finally
		{
			if (completed) mainDbConn.commit();
			else mainDbConn.rollback();
			mainDbConn.setAutoCommit(true);
		}
	}

//	 -      -%-      -

	public synchronized List<String>
	readInbox(
		String actorUsername,
		ZonedDateTime before, ZonedDateTime after,
		int pageSize)
	throws SQLException
	{
		return pageLookup(
			mainDbConn, "Inboxes", "activityId",
			"actorUsername", actorUsername,
			before, after, pageSize);
	}

	public synchronized void
	saveInInbox(String actorUsername, JsonObject activity)
	throws SQLException
	{
		String activityId = activity.getString("id", null);
		assert activityId != null;

		Timestamp created = getTimestamp(activity, "created");
		assert created != null;

		mainDbConn.setAutoCommit(false);
		boolean completed = false;
		try
		{
			PreparedStatement s = mainDbConn.prepareStatement(
				"\n INSERT" +
				"\n INTO Inboxes" +
				"\n (actorUsername, created, activityId)" +
				"\n VALUES ?, ?, ?;");
			s.setString(1, actorUsername);
			s.setTimestamp(2, created);
			s.setString(3, activityId);
			s.executeUpdate();

			completed = true;
		}
		finally
		{
			if (completed) mainDbConn.commit();
			else mainDbConn.rollback();
			mainDbConn.setAutoCommit(true);
		}
	}

//	 -      -%-      -

	public synchronized List<String>
	readOutbox(
		String actorUsername,
		ZonedDateTime before, ZonedDateTime after, int pageSize)
	throws SQLException
	{
		return pageLookup(
			mainDbConn, "Outboxes", "activityId",
			"actorUsername", actorUsername,
			before, after, pageSize);
	}

	public synchronized void
	saveInOutbox(String actorUsername, JsonObject activity)
	throws SQLException
	{
		String activityId = activity.getString("id", null);
		assert activityId != null;

		Timestamp created = getTimestamp(activity, "created");
		assert created != null;

		mainDbConn.setAutoCommit(false);
		boolean completed = false;
		try
		{
			PreparedStatement s = mainDbConn.prepareStatement(
				"\n INSERT" +
				"\n INTO Outboxes" +
				"\n (actorUsername, created, activityId)" +
				"\n VALUES ?, ?, ?;");
			s.setString(1, actorUsername);
			s.setTimestamp(2, created);
			s.setString(3, activityId);
			s.executeUpdate();

			completed = true;
		}
		finally
		{
			if (completed) mainDbConn.commit();
			else mainDbConn.rollback();
			mainDbConn.setAutoCommit(true);
		}
	}

//	 -      -%-      -

	public synchronized JsonObject
	getAttachment(String id)
	throws SQLException
	{
		String jsonr = singleLookup(
			objectDbConn, "Attachments", id);
		return jsonr != null ? toJsonObject(jsonr) : null;
	}

	public synchronized void
	saveAttachment(JsonObject attachment)
	throws SQLException
	{
		String attachmentId = attachment.getString("id", null);
		assert attachmentId != null;

		Timestamp created = getTimestamp(attachment, "created");
		assert created != null;

		objectDbConn.setAutoCommit(false);
		boolean completed = false;
		try
		{
			PreparedStatement s = objectDbConn.prepareStatement(
				"\n INSERT" +
				"\n INTO Attachments" +
				"\n (id, created, entity)" +
				"\n VALUES ?, ?, ?;");
			s.setString(1, attachmentId);
			s.setTimestamp(2, created);
			s.setString(3, attachment.toString());
			s.executeUpdate();

			completed = true;
		}
		finally
		{
			if (completed) objectDbConn.commit();
			else objectDbConn.rollback();
			objectDbConn.setAutoCommit(true);
		}
	}

//   -      -%-      -

	public synchronized Account
	getAccount(String actorUsername)
	throws SQLException
	{
		assert actorUsername != null;

		PreparedStatement s = mainDbConn.prepareStatement(
			"\n SELECT * " +
			"\n FROM Accounts" +
			"\n WHERE actorUsername = ?;");

		s.setString(1, actorUsername);
		ResultSet r = s.executeQuery();
		if (!r.next()) return null;

		Account returnee = new Account();
		returnee.actorUsername = r.getString(1);
		returnee.passphraseHash = r.getString(2);
		returnee.keyPair = parseKeyPair(
			r.getString(4), r.getString(5));

		assert returnee.actorUsername != null;
		assert returnee.passphraseHash != null;
		assert returnee.keyPair != null;

		return returnee;
	}

	public synchronized void
	createAccount(String actorUsername, String passphrase)
	throws SQLException
	{
		assert actorUsername != null;
		assert passphrase != null;

		KeyPair keypair = keypairGen.generateKeyPair();
		assert keypair.getPrivate().getFormat().equals("PKCS#8");
		assert keypair.getPublic().getFormat().equals("X.509");

		Base64.Encoder enc = Base64.getEncoder();
		String sPrivateKey = enc.encodeToString(
			keypair.getPrivate().getEncoded());
		String sPublicKey = enc.encodeToString(
			keypair.getPublic().getEncoded());

		int salt = (int)(Math.random() * Integer.MAX_VALUE);
		String hashee = passphrase + salt;
		hasher.update(hashee.getBytes());
		String hash = enc.encodeToString(hasher.digest());

		mainDbConn.setAutoCommit(false);
		boolean completed = false;
		try
		{
			PreparedStatement s = mainDbConn.prepareStatement(
				"\n INSERT" +
				"\n INTO Accounts" +
				"\n VALUES ?, ?, ?, ?, ?;");
	
			s.setString(1, actorUsername);
			s.setString(2, hash);
			s.setString(3, Integer.toString(salt));
			s.setString(4, sPrivateKey);
			s.setString(5, sPublicKey);
			s.executeUpdate();

			completed = true;
		}
		finally
		{
			if (completed) mainDbConn.commit();
			else mainDbConn.rollback();
			mainDbConn.setAutoCommit(true);
		}
	}

//	 -      -%-      -

	public synchronized boolean
	passphraseMatches(String actorUsername, String passphrase)
	throws SQLException
	{
		assert actorUsername != null;
		assert passphrase != null;

		PreparedStatement s = mainDbConn.prepareStatement(
			"\n SELECT passphraseHash, passphraseSalt" +
			"\n FROM Accounts" +
			"\n WHERE actorUsername = ?;");

		s.setString(1, actorUsername);
		ResultSet r = s.executeQuery();
		if (!r.next()) return false;
		/*
		* Assume caller called us without checking if actor
		* exists. Though they should probably know that
		* information for special error message, maybe we
		* should make them do it?
		*/
		String passphraseHash = r.getString(1);
		String passphraseSalt = r.getString(2);

		Base64.Encoder enc = Base64.getEncoder();
		String hashee = passphrase + passphraseSalt;
		hasher.update(hashee.getBytes());
		String hash = enc.encodeToString(hasher.digest());

		return hash.equals(passphraseHash);
	}

	public synchronized JsonObject
	getAccessTokenOwner(String accessToken)
	throws SQLException
	{
		assert accessToken != null;

		PreparedStatement s = mainDbConn.prepareStatement(
			"\n SELECT actorUsername" +
			"\n FROM AccessTokens" +
			"\n WHERE token = ?;");

		s.setString(1, accessToken);
		ResultSet r = s.executeQuery();
		if (!r.next()) return null;
		String actorUsername = r.getString(1);
		/*
		* Consider having this method return just the username
		* and the calling route has to do the regeneration.
		* Though regeneration as a concept might be something
		* we want to avoid.
		*/

		// (未) Invalidate access token if we're
		// past its expiry date.

		String actorId = ActivityPubUtil
			.localShortIdToId("actor", actorUsername);
		String jsonr = singleLookup(mainDbConn, "Actors", actorId);
		return jsonr != null ? toJsonObject(jsonr) : null;
	}

	public synchronized String
	createAccessToken(String actorUsername)
	throws SQLException
	{
		assert actorUsername != null;

		Base64.Encoder enc = Base64.getEncoder();
		long n = (long)(Math.random() * Long.MAX_VALUE);
		String hashee = hashCode() + actorUsername + n;
		hasher.update(hashee.getBytes());
		String accessToken = enc.encodeToString(hasher.digest());
	
		Timestamp expiryDate = Timestamp
			.from(ZonedDateTime.now().plusWeeks(1).toInstant());

		mainDbConn.setAutoCommit(false);
		boolean completed = false;
		try
		{
			PreparedStatement s = mainDbConn.prepareStatement(
				"\n INSERT" +
				"\n INTO AccessTokens" +
				"\n VALUES ?, ?, ?;");
	
			s.setString(1, actorUsername);
			s.setString(2, accessToken);
			s.setTimestamp(3, expiryDate);
			s.executeUpdate();
	
			completed = true;
			return accessToken;
		}
		finally
		{
			if (completed) mainDbConn.commit();
			else mainDbConn.rollback();
			mainDbConn.setAutoCommit(true);
		}
	}

//   -      -%-      -

	public synchronized void
	recordNewJob(
		String jobId, String summary,
		String status, ZonedDateTime startDate)
	throws SQLException
	{
		assert jobId != null;
		assert summary != null;
		assert status != null;
		assert startDate != null;

		Timestamp tStartDate = Timestamp
			.from(startDate.toInstant());

		mainDbConn.setAutoCommit(false);
		boolean completed = false;
		try
		{
			PreparedStatement s = mainDbConn.prepareStatement(
				"\n INSERT" +
				"\n INTO JobLog" +
				"\n VALUES ?, ?, ?, ?, ?;");
			s.setString(1, jobId);
			s.setString(2, status);
			s.setTimestamp(3, tStartDate);
			s.setNull(4, Types.TIMESTAMP);
			s.setString(5, summary);
			s.executeUpdate();
	
			completed = true;
		}
		finally
		{
			if (completed) mainDbConn.commit();
			else mainDbConn.rollback();
			mainDbConn.setAutoCommit(true);
		}
	}

	public synchronized void
	recordJobUpdate(
		String jobId, String newStatus,
		ZonedDateTime endDate)
	throws SQLException
	{
		assert jobId != null;
		assert newStatus != null;

		Timestamp tEndDate = null;
		if (endDate != null) tEndDate = Timestamp
			.from(endDate.toInstant());

		mainDbConn.setAutoCommit(false);
		boolean completed = false;
		try
		{
			PreparedStatement s = mainDbConn.prepareStatement(
				"\n UPDATE JobLog" +
				"\n SET status = ?, endDate = ?" +
				"\n WHERE id = ?;");

			s.setString(1, newStatus);
			if (tEndDate != null) s.setTimestamp(2, tEndDate);
			else s.setNull(2, Types.TIMESTAMP);
			s.setString(3, jobId);
			s.executeUpdate();
	
			completed = true;
		}
		finally
		{
			if (completed) mainDbConn.commit();
			else mainDbConn.rollback();
			mainDbConn.setAutoCommit(true);
		}
	}


//  -=- --- -%- --- -=-

	private KeyPair
	parseKeyPair(
		String base64PrivateKeyData, String base64PublicKeyData)
	{
		Base64.Decoder dec = Base64.getDecoder();
		byte[] privateKeyData = dec.decode(base64PrivateKeyData);
		byte[] publicKeyData = dec.decode(base64PublicKeyData);
		try
		{
			return new KeyPair(
				keyFactory.generatePublic(
					new X509EncodedKeySpec(publicKeyData)),
				keyFactory.generatePrivate(
					new PKCS8EncodedKeySpec(privateKeyData)));
		}
		catch (InvalidKeySpecException eIk)
		{
			return null;
		}
	}

//  -=- --- -%- --- -=-

	public static String
	generateObjectShortId() { return generateShortId(); }

	public static String
	generateActivityShortId() { return generateShortId(); }

//   -      -%-      -

	public static JsonObject
	toJsonObject(String jsonr)
	{
		try
		{
			StringReader s = new StringReader(jsonr);
			return Json.createReader(s).readObject();
		}
		catch (JsonParsingException eJp)
		{
			assert false;
			return null;
		}
		catch (JsonException eJ)
		{
			assert false;
			return null;
		}
	}

//  -=- --- -%- --- -=-

	private static String
	generateShortId()
	{
		long c1 = ZonedDateTime.now().toEpochSecond();
		long c2 = Thread.currentThread().getId();
		// Both of these are sensitive information, but what
		// is an attacker going to do knowing the thread..?
		String id = (c1 & 0xFFFFFFFF) + "" + c2 + (c1 >> 32);

		StringBuilder b = new StringBuilder();
		final char[] LOOKUP = "0123456789ABCDEF".toCharArray();
		for (byte c: id.getBytes())
			b.append(LOOKUP[c >> 4] + "" + LOOKUP[c & 15]);

		return b.toString();
	}

//   -      -%-      -

	private static Timestamp
	getTimestamp(JsonObject entity, String keyName)
	{
		try
		{
			String s = entity.getString(keyName, null);
			if (s == null) return null;
			ZonedDateTime t = ZonedDateTime.parse(s);
			return Timestamp.from(t.toInstant());
		}
		catch (DateTimeParseException eDt)
		{
			return null;
		}
	}

//   -      -%-      -

	private static String
	singleLookup(Connection dbConn, String tableName, String id)
	throws SQLException
	{
		assert dbConn != null;
		assert tableName != null;
		assert id != null;

		PreparedStatement s = dbConn.prepareStatement(
			"\n SELECT entity" +
			"\n FROM " + tableName +
			"\n WHERE id = ?;");

		s.setString(1, id);
		ResultSet r = s.executeQuery();
		if (!r.next()) return null;
		return r.getString(1);
	}

	private static List<String>
	pageLookup(
		Connection dbConn, String tableName, String valueName,
		String keyName, String key,
		ZonedDateTime before, ZonedDateTime after,
		int pageSize)
	throws SQLException
	{
		assert dbConn != null;
		assert tableName != null;
		assert valueName != null;
		assert pageSize > 0;

		Timestamp beforeTimestamp = null;
		Timestamp afterTimestamp = null;
		if (before != null)
			beforeTimestamp = Timestamp.from(before.toInstant());
		if (after != null)
			afterTimestamp = Timestamp.from(after.toInstant());

		String keyLine = "";
		String beforeLine = "";
		String afterLine = "";
		int keyParamIndex = 0;
		int beforeParamIndex = 0;
		int afterParamIndex = 0;

		int lastParamIndex = 0;
		if (key != null)
		{
			keyLine = "\n AND " + keyName + " = ?";
			keyParamIndex = ++lastParamIndex;
			if (keyParamIndex == 1)
				keyLine = keyLine.replace("AND", "WHERE");
		}
		if (before != null)
		{
			beforeLine = "\n AND created < ?";
			beforeParamIndex = ++lastParamIndex;
			if (beforeParamIndex == 1)
				beforeLine = beforeLine.replace("AND", "WHERE");
		}
		if (after != null)
		{
			afterLine = "\n AND created > ?";
			afterParamIndex = ++lastParamIndex;
			if (afterParamIndex == 1)
				afterLine = afterLine.replace("AND", "WHERE");
		}
		
		PreparedStatement s = dbConn.prepareStatement(
			"\n SELECT " + valueName +
			"\n FROM " + tableName +
			keyLine + beforeLine + afterLine +
			"\n ORDER BY created DESC;");
		if (!keyLine.isEmpty())
			s.setString(keyParamIndex, key);
		if (!beforeLine.isEmpty())
			s.setTimestamp(beforeParamIndex, beforeTimestamp);
		if (!afterLine.isEmpty())
			s.setTimestamp(afterParamIndex, afterTimestamp);
		s.setMaxRows(pageSize);
		
		List<String> returnee = new ArrayList<>();
		ResultSet r = s.executeQuery();
		while (r.next()) returnee.add(r.getString(1));
		return returnee;
	}



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



	public static class
	Account {
	
		String
		actorUsername;
	
		String
		passphraseHash;
	
		KeyPair
		keyPair;
	
	}



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



	public static void
	main(String... args)
	throws SQLException, IOException
	{
		if (args.length == 2)
		if (args[0].equals("server"))
		{
			ConfigAPI.DbConnProfile profile; try
			{
				ConfigAPI c = new ConfigAPI();
				profile = c.readDbConnProfile(args[1]);
				if (profile == null) return;
			}
			catch (ConfigAPI.ConfigException eCon)
			{
				eCon.printStackTrace();
				return;
			}

			startHsqlServer(profile);
			// With the server not shut down, this JVM should
			// remain alive at this point, until interrupted.
		}

		if (args.length == 4)
		if (args[0].equals("script"))
		{
			ConfigAPI.DbConnProfile profile; try
			{
				ConfigAPI c = new ConfigAPI();
				profile = c.readDbConnProfile(args[1]);
				if (profile == null) return;
			}
			catch (ConfigAPI.ConfigException eCon)
			{
				eCon.printStackTrace();
				return;
			}

			boolean main = args[2].equals("main");
			boolean object = args[2].equals("object");
			if (!(main || object)) return;

			DataAPI instance = new DataAPI(profile);
			Connection conn = null;
			if (main) conn = instance.mainDbConn;
			else if (object) conn = instance.objectDbConn;

			System.err.print("Running " + args[3]);
			System.err.println(" on " + args[2] + " DB.");
			runSQLScript(conn, args[3]);

			instance.close();
			/*
			* Closing a connection is what causes it to write
			* to disk. Apparently. And unlike HTTPServer's usual
			* running, where if you interrupt the process an
			* interrupt handler closes the connection for us,
			* we don't have that here, so close it ourselves.
			* (You can also just enter an infinite loop if you
			* want the user to interrupt and make the handler do
			* it for us.)
			*/
			return;
		}
	}

	public static void
	runSQLScript(Connection conn, String filepath)
	throws SQLException, IOException
	{
		BufferedReader r = new BufferedReader(
			new FileReader(filepath));

		StringBuilder b = new StringBuilder();
		int n = 0;
		String line; while ((line = r.readLine()) != null)
		{
			b.append("\n" + line);
			if (line.endsWith(";"))
			{
				String sql = b.toString();
				b.setLength(0);

				conn.createStatement().execute(sql);
				conn.commit();
				++n;
			}
		}

		System.err.println("Ran " + n + " statements in script.");
	}

	public void
	close()
	throws SQLException
	{
		mainDbConn.close();
		objectDbConn.close();
	}



// -=- - --%- -%-- - -=-
//        %  @  %
// -=- - --%- -%-- - -=-



	public
	DataAPI(ConfigAPI.DbConnProfile profile)
	throws SQLException
	{
		prepareDbConns(profile);

		if (!profile.ignoreDatabaseVersion)
			checkDatabaseVersion(16, 16);

		initCommon();
	}

//  -=- --- -%- --- -=-

	private void
	prepareDbConns(ConfigAPI.DbConnProfile profile)
	throws SQLException
	{
		mainDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:" + profile.specPrefix + "main",
			profile.username, profile.passphrase);

		objectDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:" + profile.specPrefix + "object",
			profile.username, profile.passphrase);

		try
		{
			for (String path: profile.mainInitScripts)
				runSQLScript(mainDbConn, path);

			for (String path: profile.objectInitScripts)
				runSQLScript(objectDbConn, path);
		}
		catch (IOException eIo)
		{
			throw new SQLException(
				"Couldn't read a DB init script file", eIo);
		}
	}

	private void
	checkDatabaseVersion(
		int expectedMainDbVersion, int expectedObjectDbVersion)
	throws SQLException
	{
		int mainDbVersion = getDatabaseVersion(mainDbConn);
		int objectDbVersion = getDatabaseVersion(objectDbConn);

		boolean mainOk =
			mainDbVersion == expectedMainDbVersion;
		boolean objectOk =
			objectDbVersion == expectedObjectDbVersion;
		if (!mainOk || !objectOk)
			throw new SQLException(
				"One of the database versions does " +
				"not match DataAPI's compatibility");
	}

	private void
	initCommon()
	{
		try
		{
			keypairGen = KeyPairGenerator.getInstance("RSA");
			hasher = MessageDigest.getInstance("SHA-256");
			keyFactory = KeyFactory.getInstance("RSA");
		}
		catch (NoSuchAlgorithmException eNsa)
		{
			assert false;
		}
	}

//	-=- --- -%- --- -=-

	private static void
	startHsqlServer(ConfigAPI.DbConnProfile profile)
	throws SQLException
	{
		String specSuffix = "";
		specSuffix += ";user=" + profile.username;
		specSuffix += ";password=" + profile.passphrase;
		/*
		* Unlike in-memory databases, when Server#start is
		* invoked the initial user is created. If we don't
		* specify for hsqldb.server.Server through a suffix
		* in the path it's going to use the default "SA".
		*/

		String mainSpec =
			profile.specPrefix + "main" + specSuffix;
		String objectSpec =
			profile.specPrefix + "object" + specSuffix;

		Server hsqlServer = new Server();
		hsqlServer.setDatabasePath(0, mainSpec);
		hsqlServer.setDatabaseName(0, "gemetar_main");
		hsqlServer.setDatabasePath(1, objectSpec);
		hsqlServer.setDatabaseName(1, "gemetar_object");
		hsqlServer.start();

		/*
		int OPENING = ServerProperties.SERVER_STATE_OPENING;
		int ONLINE = ServerProperties.SERVER_STATE_ONLINE;
		/*/
		int OPENING = 4, ONLINE = 1;
		// For some reason hsqldb.server.ServerProperties lacks
		// these properties as public at the moment. Following
		// what the Javadoc says.
		//*/
		while (hsqlServer.getState() == OPENING);
		if (hsqlServer.getState() != ONLINE)
		{
			throw new SQLException(
				"HSQLDB server failed to start: "
				+ hsqlServer.getStateDescriptor());
		}

		/*
		* Starting the HTTP server doesn't run init scripts.
		* Instead the first client connection using DataAPI's
		* constructors will execute the scripts instead.
		* Carefully pick the matching profile.
		*/
	}

	private static int
	getDatabaseVersion(Connection conn)
	throws SQLException
	{
		PreparedStatement s1 = conn.prepareStatement(
			"\n SELECT 1" +
			"\n FROM Information_Schema.Tables" +
			"\n WHERE TABLE_NAME = 'SERVERINFO';");
		ResultSet r1 = s1.executeQuery();
		if (!r1.next()) return 0;

		PreparedStatement s2 = conn.prepareStatement(
			"\n SELECT databaseVersion" +
			"\n FROM ServerInfo;");
		ResultSet r2 = s2.executeQuery();
		assert r2.next();
		return r2.getInt(1);
	}

}
