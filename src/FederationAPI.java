
import java.sql.SQLException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import javax.json.JsonObject;
import javax.json.Json;
import javax.json.JsonException;
import javax.json.stream.JsonParsingException;
import java.io.StringReader;

class
WebfingerRoute implements Route {

	DataAPI
	dataAPI;

	Logger
	logger;

//	---%-@-%---

	public String
	getMethod() { return "GET"; }

	public String
	getPath() { return "/.well-known/webfinger"; }

	public GemetarRouteRequirements
	getRouteRequirements()
	{
		GemetarRouteRequirements returnee =
			new GemetarRouteRequirements();

		returnee.requiredQueryParams = new String[]
			{ "resource" };

		return returnee;
	}

	public HTTPResponse
	invoke(HTTPRequest request)
	throws SQLException
	{
		HTTPResponse response = new HTTPResponse();
		GemetarRouteRequirements req = getRouteRequirements();
		boolean ok = Gemetar
			.enforceRouteRequirements(req, request, response);
		if (!ok) return response;


		String subject = request.params.get("resource");
		String username = subjectToUsername(subject);
		if (username == null)
		{
			HTTPResponse returnee = new HTTPResponse();
			returnee.code = 400;
			returnee.body =
				"resource param given does not conform to"
				+ "resource spec.. It goes like this:"
				+ " acct:<username>@<host>, where the only"
				+ " host this server knows is itself.";
			return returnee;
		}


		JsonObject actorEntity = null; do
		{
			GetLocalActorRoute fRoute = new GetLocalActorRoute
				(dataAPI, logger);

			HTTPRequest fRequest = new HTTPRequest();
			fRequest.params.put("\b1", username);

			HTTPResponse fResponse = fRoute.invoke(fRequest);
			if (fResponse.code != 200) break;

			actorEntity = DataAPI.toJsonObject(fResponse.body);
		}
		while (false); if (actorEntity == null)
		{
			HTTPResponse returnee = new HTTPResponse();
			returnee.code = 404;
			returnee.body = "Actor not found.";
			return returnee;
		}
		String actorId = actorEntity.getString("id", null);
		assert actorId != null;


		JsonObject webfingerEntity = Json.createObjectBuilder()
			.add("subject", subject)
			.add("links", Json.createArrayBuilder()
				.add(Json.createObjectBuilder()
					.add("rel", "self")
					.add("type", "application/activity+json")
					.add("href", actorId)))
			.build();


		HTTPResponse returnee = new HTTPResponse();
		returnee.method = request.method;
		returnee.uri = request.uri;
		returnee.code = 200;
		returnee.headers.put("Content-Type", "application/json");
		returnee.body = webfingerEntity.toString();
		return returnee;
	}

	public static String
	subjectToUsername(String subject)
	{
		final String HOST = "localhost";
		String regex = "acct:(.+)@" + HOST;
		Matcher m = Pattern.compile(regex).matcher(subject);
		if (!m.matches()) return null;
		return m.group(1);
	}

//	---%-@-%---

	WebfingerRoute(DataAPI dataAPI, Logger logger)
	{
		this.dataAPI = dataAPI;
		this.logger = logger;
	}

}



class
GetLocalActorRoute implements Route {

	DataAPI
	dataAPI;

	Logger
	logger;

//	---%-@-%---

	public String
	getMethod() { return "GET"; }

	public String
	getPath() { return "/actors/<>"; }

	public HTTPResponse
	invoke(HTTPRequest request)
	throws SQLException
	{
		String username = request.params.get("\b1");

		String id = ActivityPubUtil
			.localShortIdToId("actor", username);

		JsonObject inDb = dataAPI.getActor(id);
		if (inDb == null)
		{
			// For now we don't regenerate actors.
			HTTPResponse returnee = new HTTPResponse();
			returnee.code = 404;
			returnee.body = "Actor not found.";
			return returnee;
		}

		inDb = Entities.filterPrivateFromActor(inDb);

		HTTPResponse returnee = new HTTPResponse();
		returnee.method = request.method;
		returnee.uri = request.uri;
		returnee.code = 200;
		returnee.headers.put("Content-Type", "application/json");
		returnee.body = inDb.toString();
		return returnee;
	}

//	---%-@-%---

	GetLocalActorRoute(DataAPI dataAPI, Logger logger)
	{
		this.dataAPI = dataAPI;
		this.logger = logger;
	}

}



class
GetLocalObjectRoute implements Route {

	DataAPI
	dataAPI;

	Logger
	logger;

//	---%-@-%---

	public String
	getMethod() { return "GET"; }

	public String
	getPath() { return "/objects/<>"; }

	public HTTPResponse
	invoke(HTTPRequest request)
	throws SQLException
	{
		String shortId = request.params.get("\b1");

		String id = ActivityPubUtil
			.localShortIdToId("object", shortId);

		JsonObject inDb = dataAPI.getObject(id);
		if (inDb == null)
		{
			HTTPResponse returnee = new HTTPResponse();
			returnee.code = 404;
			returnee.body = "Object not found.";
			return returnee;
		}

		inDb = Entities.filterPrivateFromObject(inDb);

		HTTPResponse returnee = new HTTPResponse();
		returnee.method = request.method;
		returnee.uri = request.uri;
		returnee.code = 200;
		returnee.headers.put("Content-Type", "application/json");
		returnee.body = inDb.toString();
		return returnee;
	}

//	---%-@-%---

	GetLocalObjectRoute(DataAPI dataAPI, Logger logger)
	{
		this.dataAPI = dataAPI;
		this.logger = logger;
	}

}



class
GetLocalActivityRoute implements Route {

	DataAPI
	dataAPI;

	Logger
	logger;

//	---%-@-%---

	public String
	getMethod() { return "GET"; }

	public String
	getPath() { return "/activities/<>"; }

	public HTTPResponse
	invoke(HTTPRequest request)
	throws SQLException
	{
		String shortId = request.params.get("\b1");

		String id = ActivityPubUtil
			.localShortIdToId("activity", shortId);

		JsonObject inDb = dataAPI.getActivity(id);
		if (inDb == null)
		{
			HTTPResponse returnee = new HTTPResponse();
			returnee.code = 404;
			returnee.body = "Activity not found.";
			return returnee;
		}

		inDb = Entities.filterPrivateFromActivity(inDb);

		HTTPResponse returnee = new HTTPResponse();
		returnee.method = request.method;
		returnee.uri = request.uri;
		returnee.code = 200;
		returnee.headers.put("Content-Type", "application/json");
		returnee.body = inDb.toString();
		return returnee;
	}

//	---%-@-%---

	GetLocalActivityRoute(DataAPI dataAPI, Logger logger)
	{
		this.dataAPI = dataAPI;
		this.logger = logger;
	}

}



class
GetLocalAttachmentRoute implements Route {

	DataAPI
	dataAPI;

	Logger
	logger;

//	---%-@-%---

	public String
	getMethod() { return "GET"; }

	public String
	getPath() { return "/attachments/<>"; }

	public HTTPResponse
	invoke(HTTPRequest request)
	throws SQLException
	{
		String shortId = request.params.get("\b1");

		String id = ActivityPubUtil
			.localShortIdToId("attachment", shortId);

		JsonObject inDb = dataAPI.getAttachment(id);
		if (inDb == null)
		{
			HTTPResponse returnee = new HTTPResponse();
			returnee.code = 404;
			returnee.body = "Attachment not found.";
			return returnee;
		}

		inDb = Entities.filterPrivateFromAttachment(inDb);

		HTTPResponse returnee = new HTTPResponse();
		returnee.method = request.method;
		returnee.uri = request.uri;
		returnee.code = 200;
		returnee.headers.put("Content-Type", "application/json");
		returnee.body = inDb.toString();
		return returnee;
	}

//	---%-@-%---

	GetLocalAttachmentRoute(DataAPI dataAPI, Logger logger)
	{
		this.dataAPI = dataAPI;
		this.logger = logger;
	}

}



class
SendIntoInboxRoute implements Route {

	DataAPI
	dataAPI;

	Logger
	logger;

//	---%-@-%---

	public String
	getMethod() { return "GET"; }

	public String
	getPath() { return "/actors/<>/inbox"; }

	public GemetarRouteRequirements
	getRouteRequirements()
	{
		GemetarRouteRequirements returnee =
			new GemetarRouteRequirements();

		returnee.authRequired = true;
		returnee.dataAPI = this.dataAPI;

		return returnee;
	}

	public HTTPResponse
	invoke(HTTPRequest request)
	throws SQLException
	{
		HTTPResponse response = new HTTPResponse();
		GemetarRouteRequirements req = getRouteRequirements();
		boolean ok = Gemetar
			.enforceRouteRequirements(req, request, response);
		if (!ok) return response;

		response.method = request.method;
		response.uri = request.uri;
		response.code = 500;
		return response;
	}

//	---%-@-%---

	SendIntoInboxRoute(DataAPI dataAPI, Logger logger)
	{
		this.dataAPI = dataAPI;
		this.logger = logger;
	}

}
