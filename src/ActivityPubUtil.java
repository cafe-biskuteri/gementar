
import java.time.ZonedDateTime;
import java.security.KeyPair;
import javax.json.Json;
import javax.json.JsonValue;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonArray;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;


class
ActivityPubUtil {

	public static final JsonValue
	basicContext;

//	---%-@-%---

	public static String
	localShortIdToId(String type, String shortId)
	{
		if (shortId == null) return null;

		final String HOST = "localhost";
		String prefix = "https://" + HOST + "/";
		if (type.equals("activity")) prefix += "activities/";
		else if (type.equals("actor")) prefix += "actors/";
		else if (type.equals("object")) prefix += "objects/";
		else if (type.equals("attachment"))
			prefix += "attachments/";
		else return null;

		return prefix + shortId;
	}

	public static String
	localIdToShortId(String id)
	{
		if (id == null) return null;

		final String HOST = "localhost";
		String regex = "https://" + HOST + "/.+/(.+)";
		Matcher m = Pattern.compile(regex).matcher(id);
		if (!m.matches()) return null;
		return m.group(1);
	}


	public static boolean
	contentTypeOk(String contentType)
	{
		if (contentType == null) return false;

		String[] accepted = new String[] {
			"application/json",
			"application/activity+json" };
		for (String contentType2: accepted)
			if (contentType.equals(contentType2))
				return true;

		return false;
	}


	public static JsonObjectBuilder
	edit(JsonObject json)
	{
		JsonObjectBuilder returnee = Json.createObjectBuilder();
		for (String key: json.keySet())
			returnee.add(key, json.get(key));
		return returnee;
	}

	public static JsonObjectBuilder
	merge(JsonObject json1, JsonObject json2)
	{
		JsonObjectBuilder b = edit(json1);
		for (String key: json2.keySet())
		{
			if (json1.containsKey(key)) continue;
			b.add(key, json2.get(key));
		}
		return b;
	}

//	---%-@-%---

	static {
		basicContext = Json.createArrayBuilder()
			.add("https://www.w3.org/ns/activitystreams")
			.build()
			.get(0);
	}

}
