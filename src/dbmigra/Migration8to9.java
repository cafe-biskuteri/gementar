
import java.sql.*;
import java.util.regex.*;
import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.io.*;

class
Migration8to9 {

	public static void
	main(String... args)
	throws SQLException, JsonParsingException, JsonException
	{
		Connection mainDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/main", "Rani", "");
		Connection objectDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/object", "Rani", "");

		mainDbConn.setAutoCommit(false);
		objectDbConn.setAutoCommit(false);

		addColumn(objectDbConn);
		fillColumnData(objectDbConn);
		dropTable(objectDbConn);
		changeVersion(mainDbConn, objectDbConn);

		mainDbConn.close();
		objectDbConn.close();
	}

	public static void
	addColumn(Connection objectDbConn)
	throws SQLException
	{
		PreparedStatement s1 = objectDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE AttachmentsParams" +
			"\n ADD COLUMN IF NOT EXISTS" +
			"\n data BLOB" +
			"\n DEFAULT NULL;");
		s1.executeUpdate();

		objectDbConn.commit();
	}

	public static void
	fillColumnData(Connection objectDbConn)
	throws SQLException
	{
		PreparedStatement s1 = objectDbConn.prepareStatement(
			"\n SELECT *" +
			"\n FROM AttachmentsContents;");
		PreparedStatement s2 = objectDbConn.prepareStatement(
			"\n UPDATE AttachmentsParams" +
			"\n SET data = ?" +
			"\n WHERE shortId = ?;");
		ResultSet r1 = s1.executeQuery();
		while (r1.next())
		{
			s2.setString(2, r1.getString("shortId"));
			s2.setBlob(1, r1.getBlob("data"));
			s2.executeUpdate();
		}

		objectDbConn.commit();
	}

	public static void
	dropTable(Connection objectDbConn)
	throws SQLException
	{
		PreparedStatement s1 = objectDbConn.prepareStatement(
			"\n DROP" +
			"\n TABLE AttachmentsContents;");
		s1.executeUpdate();

		objectDbConn.commit();
	}

	public static void
	changeVersion(Connection mainDbConn, Connection objectDbConn)
	throws SQLException
	{
		PreparedStatement s = mainDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 9;");
		s.executeUpdate();

		s = objectDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 9;");
		s.executeUpdate();

		mainDbConn.commit();
		objectDbConn.commit();
	}

//	 -	-%-	 -

	private static JsonObject
	toJsonObject(String string)
	throws JsonParsingException, JsonException
	{
		return Json.createReader(new StringReader(string))
			.readObject();
	}

}
