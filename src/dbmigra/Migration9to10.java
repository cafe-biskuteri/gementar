
import java.sql.*;
import java.util.regex.*;
import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.io.*;

class
Migration9to10 {

	public static void
	main(String... args)
	throws SQLException, JsonParsingException, JsonException
	{
		Connection mainDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/main", "Rani", "");
		Connection objectDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/object", "Rani", "");

		mainDbConn.setAutoCommit(false);
		objectDbConn.setAutoCommit(false);

		initialAlters(mainDbConn, objectDbConn);
		moveData(mainDbConn);
		dropTables(mainDbConn);
		changeVersion(mainDbConn, objectDbConn);

		mainDbConn.close();
		objectDbConn.close();
	}

	public static void
	initialAlters(Connection mainDbConn, Connection objectDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE ActorsCache" +
			"\n RENAME TO HTTPCache;");
		s1.executeUpdate();

		PreparedStatement s2 = objectDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE AttachmentsCache" +
			"\n RENAME TO HTTPCache;");
		s2.executeUpdate();

		mainDbConn.commit();
		objectDbConn.commit();
	}

	public static void
	moveData(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n SELECT *" +
			"\n FROM ObjectsCache;");
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n SELECT *" +
			"\n FROM ActivitiesCache;");
		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n INSERT" +
			"\n INTO HTTPCache" +
			"\n VALUES (?, ?);");

		ResultSet r1 = s1.executeQuery();
		while (r1.next())
		{
			s3.setString(1, r1.getString(1));
			s3.setString(2, r1.getString(2));
			s3.executeUpdate();
		}

		ResultSet r2 = s2.executeQuery();
		while (r2.next())
		{
			s3.setString(1, r2.getString(1));
			s3.setString(2, r2.getString(2));
			s3.executeUpdate();
		}

		mainDbConn.commit();
	}

	public static void
	dropTables(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(	
			"\n DROP" +
			"\n TABLE ObjectsCache;");
		s1.executeUpdate();

		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n DROP" +
			"\n TABLE ActivitiesCache;");
		s2.executeUpdate();

		mainDbConn.commit();
	}

	public static void
	changeVersion(Connection mainDbConn, Connection objectDbConn)
	throws SQLException
	{
		PreparedStatement s = mainDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 10;");
		s.executeUpdate();

		s = objectDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 10;");
		s.executeUpdate();

		mainDbConn.commit();
		objectDbConn.commit();
	}

//	 -	-%-	 -

	private static JsonObject
	toJsonObject(String string)
	throws JsonParsingException, JsonException
	{
		return Json.createReader(new StringReader(string))
			.readObject();
	}

}
