
import java.sql.*;
import java.util.regex.*;
import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.io.*;

class
Migration7to8 {

	public static void
	main(String... args)
	throws SQLException, JsonParsingException, JsonException
	{
		Connection mainDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/main", "Rani", "");
		Connection objectDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/object", "Rani", "");

		mainDbConn.setAutoCommit(false);
		objectDbConn.setAutoCommit(false);

		renameCacheTables(mainDbConn);
		addParamsTables(mainDbConn);
		fillParamsTables(mainDbConn);
		// I'm also supposed to regenerate the IDs for the actors,
		// since the format has changed now that usernames are
		// the short ID. But I'm lazy..
		addColumnsToAccountTables(mainDbConn);
		fillAccountTables(mainDbConn);
		removeColumnsFromCacheTables(mainDbConn);
		removeColumnsFromAccountTables(mainDbConn);
		changeVersion(mainDbConn);

		mainDbConn.close();
		objectDbConn.close();
	}

	public static void
	renameCacheTables(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Actors" +
			"\n RENAME TO ActorsCache;");
		s1.executeUpdate();

		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Objects" +
			"\n RENAME TO ObjectsCache;");
		s2.executeUpdate();

		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Activities" +
			"\n RENAME TO ActivitiesCache;");
		s3.executeUpdate();

		mainDbConn.commit();
	}

	public static void
	addParamsTables(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n CREATE" +
			"\n TABLE ActorsParams" +
			"\n (username VARCHAR(128)," +
			"\n entity VARCHAR(2048));");
		s1.executeUpdate();

		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n CREATE" +
			"\n TABLE ObjectsParams" +
			"\n (shortId VARCHAR(128)," +
			"\n entity VARCHAR(2048));");
		s2.executeUpdate();

		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n CREATE" +
			"\n TABLE ActivitiesParams" +
			"\n (shortId VARCHAR(128)," +
			"\n entity VARCHAR(2048));");
		s3.executeUpdate();

		mainDbConn.commit();
	}

	public static void
	fillParamsTables(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n SELECT *" +
			"\n FROM ActorsCache;");
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n INSERT" +
			"\n INTO ActorsParams" +
			"\n VALUES (?, ?);");
		ResultSet r1 = s1.executeQuery();
		while (r1.next())
		{
			s2.setString(1, r1.getString(2));
			s2.setString(2, r1.getString(4));
			s2.executeUpdate();
		}

		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n SELECT *" +
			"\n FROM ObjectsCache;");
		PreparedStatement s4 = mainDbConn.prepareStatement(
			"\n INSERT" +
			"\n INTO ObjectsParams" +
			"\n VALUES (?, ?);");
		ResultSet r3 = s3.executeQuery();
		while (r3.next())
		{
			s4.setString(1, r3.getString(1));
			s4.setString(2, r3.getString(3));
			s4.executeUpdate();
		}

		PreparedStatement s5 = mainDbConn.prepareStatement(
			"\n SELECT *" +
			"\n FROM ActivitiesCache;");
		PreparedStatement s6 = mainDbConn.prepareStatement(
			"\n INSERT" +
			"\n INTO ActivitiesParams" +
			"\n VALUES (?, ?);");
		ResultSet r5 = s5.executeQuery();
		while (r5.next())
		{
			s6.setString(1, r5.getString(1));
			s6.setString(2, r5.getString(3));
			s6.executeUpdate();
		}

		mainDbConn.commit();
	}

	public static void
	addColumnsToAccountTables(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Accounts" +
			"\n ADD COLUMN" +
			"\n actorUsername VARCHAR(128)" +
			"\n DEFAULT NULL" +
			"\n BEFORE actorShortId;");
		s1.executeUpdate();

		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE AccessTokens" +
			"\n ADD COLUMN" +
			"\n actorUsername VARCHAR(128)" +
			"\n DEFAULT NULL" +
			"\n BEFORE actorShortId;");
		s2.executeUpdate();

		mainDbConn.commit();
	}

	public static void
	fillAccountTables(Connection mainDbConn)
	throws SQLException, JsonParsingException, JsonException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n SELECT *" +
			"\n FROM ActorsCache;");
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n UPDATE Accounts" +
			"\n SET actorUsername = ?" +
			"\n WHERE actorShortId = ?;");
		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n UPDATE AccessTokens" +
			"\n SET actorUsername = ?" +
			"\n WHERE actorShortId = ?;");
		ResultSet r1 = s1.executeQuery();
		while (r1.next())
		{
			JsonObject params = toJsonObject(r1.getString(4));

			s2.setString(1, params.getString("username"));
			s2.setString(2, params.getString("shortId"));
			s2.executeUpdate();

			s3.setString(1, params.getString("username"));
			s3.setString(2, params.getString("shortId"));
			s3.executeUpdate();
		}

		mainDbConn.commit();
	}

	public static void
	removeColumnsFromCacheTables(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE ActorsCache" +
			"\n DROP COLUMN username RESTRICT;");
		s1.executeUpdate();

		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE ActorsCache" +
			"\n DROP COLUMN constructionParams RESTRICT;");
		s2.executeUpdate();

		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE ObjectsCache" +
			"\n DROP COLUMN constructionParams RESTRICT;");
		s3.executeUpdate();

		PreparedStatement s4 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE ActivitiesCache" +
			"\n DROP COLUMN constructionParams RESTRICT;");
		s4.executeUpdate();

		mainDbConn.commit();
	}

	public static void
	removeColumnsFromAccountTables(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Accounts" +
			"\n DROP COLUMN actorShortId RESTRICT;");
		s1.executeUpdate();

		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE AccessTokens" +
			"\n DROP COLUMN actorShortId RESTRICT;");
		s2.executeUpdate();
	}

	public static void
	changeVersion(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s = mainDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 8;");
		s.executeUpdate();

		mainDbConn.commit();
	}

//	 -	-%-	 -

	private static JsonObject
	toJsonObject(String string)
	throws JsonParsingException, JsonException
	{
		return Json.createReader(new StringReader(string))
			.readObject();
	}

}
