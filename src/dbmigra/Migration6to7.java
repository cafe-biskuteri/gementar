
import java.sql.*;
import java.util.regex.*;
import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.io.*;

class
Migration6to7 {

	public static void
	main(String... args)
	throws SQLException, JsonParsingException, JsonException
	{
		Connection mainDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/main", "Rani", "");
		Connection objectDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/object", "Rani", "");

		mainDbConn.setAutoCommit(false);
		objectDbConn.setAutoCommit(false);

		initialAlters(mainDbConn);
		fillActorsValues(mainDbConn);
		fillObjectsValues(mainDbConn);
		fillActivitiesValues(mainDbConn);
		finalAlters(mainDbConn);
		changeVersion(mainDbConn);

		mainDbConn.close();
		objectDbConn.close();
	}

	public static void
	initialAlters(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Actors" +
			"\n ADD COLUMN IF NOT EXISTS" +
			"\n constructionParams VARCHAR(1024)" +
			"\n DEFAULT NULL;");
		s1.executeUpdate();

		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Objects" +
			"\n ADD COLUMN IF NOT EXISTS" +
			"\n constructionParams VARCHAR(1024)" +
			"\n DEFAULT NULL;");
		s2.executeUpdate();

		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Activities" +
			"\n ADD COLUMN IF NOT EXISTS" +
			"\n constructionParams VARCHAR(1024)" +
			"\n DEFAULT NULL;");
		s3.executeUpdate();

		mainDbConn.commit();
	}

	public static void
	fillActorsValues(Connection mainDbConn)
	throws SQLException, JsonParsingException, JsonException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"SELECT * FROM Actors;");
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n UPDATE Actors" +
			"\n SET constructionParams = ?" +
			"\n WHERE shortId = ?;");
		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n UPDATE Actors" +
			"\n SET id = ?;");

		ResultSet r1 = s1.executeQuery();
		while (r1.next())
		{
			JsonObject entity = toJsonObject(r1.getString("entity"));

			String id = entity.getString("id");
			String shortId = ActivityPubUtil.localIdToShortId(id);
			JsonObject params = Json.createObjectBuilder()
				.add("shortId", shortId)
				.add("username", entity.getString("username"))
				.add("name", entity.getString("name"))
				.build();

			s2.setString(2, shortId);
			s2.setString(1, params.toString());
			s2.executeUpdate();
			s3.setString(1, id);
			s3.executeUpdate();
		}

		mainDbConn.commit();
	}

	public static void
	fillObjectsValues(Connection mainDbConn)
	throws SQLException, JsonParsingException, JsonException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"SELECT * FROM Objects;");
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n UPDATE Objects" +
			"\n SET constructionParams = ?" +
			"\n WHERE shortId = ?;");
		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n UPDATE Objects" +
			"\n SET id = ?;");
		
		ResultSet r1 = s1.executeQuery();
		while (r1.next())
		{
			JsonObject entity = toJsonObject(r1.getString("entity"));

			String id = entity.getString("id");
			String shortId = ActivityPubUtil.localIdToShortId(id);
			JsonObject params = Json.createObjectBuilder()
				.add("shortId", shortId)
				.add("type", entity.getString("type"))
				.add("created", entity.getString("created"))
				.build();

			s2.setString(2, shortId);
			s2.setString(1, params.toString());
			s2.executeUpdate();
			s3.setString(1, id);
			s3.executeUpdate();
		}

		mainDbConn.commit();
	}

	public static void
	fillActivitiesValues(Connection mainDbConn)
	throws SQLException, JsonParsingException, JsonException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"SELECT * FROM Activities;");
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n UPDATE Activities" +
			"\n SET constructionParams = ?" +
			"\n WHERE shortId = ?;");
		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n UPDATE Activities" +
			"\n SET id = ?;");

		ResultSet r1 = s1.executeQuery();
		while (r1.next()) 
		{
			JsonObject entity = toJsonObject(r1.getString("entity"));

			String id = entity.getString("id");
			String actorId = entity.getString("actor");
			String objectId = entity.getString("object");
			String shortId =
				ActivityPubUtil.localIdToShortId(id);

			String actorShortId =
				ActivityPubUtil.localIdToShortId(actorId);
			String objectShortId =
				ActivityPubUtil.localIdToShortId(objectId);
			if (actorShortId != null)
				actorId = ActivityPubUtil.localShortIdToId(
					"actors", actorShortId);
			if (objectShortId != null)
				objectId = ActivityPubUtil.localShortIdToId(
					"objects", objectShortId);

			JsonObject params = Json.createObjectBuilder()
				.add("shortId", shortId)
				.add("actorId", actorId)
				.add("objectId", objectId)
				.add("type", entity.getString("type"))
				.add("created", entity.getString("created"))
				.build();

			s2.setString(2, shortId);
			s2.setString(1, params.toString());
			s2.executeUpdate();
			s3.setString(1, id);
			s3.executeUpdate();
		}

		mainDbConn.commit();
	}

	public static void
	finalAlters(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Actors" +
			"\n DROP COLUMN shortId RESTRICT;");
		s1.executeUpdate();

		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Objects" +
			"\n DROP COLUMN shortId RESTRICT;");
		s2.executeUpdate();

		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Activities" +
			"\n DROP COLUMN shortId RESTRICT;");
		s3.executeUpdate();

		mainDbConn.commit();
	}

	public static void
	changeVersion(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s = mainDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 7;");
		s.executeUpdate();

		mainDbConn.commit();
	}

//	 -	-%-	 -

	private static JsonObject
	toJsonObject(String string)
	throws JsonParsingException, JsonException
	{
		return Json.createReader(new StringReader(string))
			.readObject();
	}

}