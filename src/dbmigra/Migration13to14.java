
import java.sql.*;
import java.util.regex.*;
import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.time.*;
import java.time.format.DateTimeParseException;
import java.io.*;

class
Migration13to14 {

	public static void
	main(String... args)
	throws SQLException, JsonParsingException, JsonException
	{
		Connection mainDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/main", "Rani", "");
		Connection objectDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/object", "Rani", "");

		mainDbConn.setAutoCommit(false);
		objectDbConn.setAutoCommit(false);

		addTables(mainDbConn, objectDbConn);
		changeVersion(mainDbConn, objectDbConn);

		mainDbConn.close();
		objectDbConn.close();
	}

	public static void
	addTables(Connection mainDbConn, Connection objectDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n CREATE" +
			"\n TABLE Outboxes" +
			"\n (actorUsername VARCHAR(128)," +
			"\n created TIMESTAMP," +
			"\n activityId VARCHAR(128));");
		s1.executeUpdate();

		mainDbConn.commit();
		objectDbConn.commit();
	}

	public static void
	changeVersion(Connection mainDbConn, Connection objectDbConn)
	throws SQLException
	{
		PreparedStatement s = mainDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 14;");
		s.executeUpdate();

		s = objectDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 14;");
		s.executeUpdate();

		mainDbConn.commit();
		objectDbConn.commit();
	}

}