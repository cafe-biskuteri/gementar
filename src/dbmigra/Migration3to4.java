
import java.sql.*;
import java.util.regex.*;
import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.io.*;

class
Migration3to4 {

	public static void
	main(String... args)
	throws SQLException
	{
		Connection mainDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/main", "Rani", "");
		Connection objectDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/object", "Rani", "");

		mainDbConn.setAutoCommit(false);
		objectDbConn.setAutoCommit(false);

		initialAlters(mainDbConn);
		fillUsernames(mainDbConn);
		changeIdsInAccountsToShortIds(mainDbConn);
		changeIdsInAccessTokensToShortIds(mainDbConn);
		finalAlters(mainDbConn);
		changeVersion(mainDbConn);

		mainDbConn.close();
		objectDbConn.close();
		// Will only run if everything passed.
	}

	public static void
	initialAlters(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Actors" +
			"\n ADD COLUMN IF NOT EXISTS" +
			"\n username VARCHAR(128)" +
			"\n DEFAULT NULL" +
			"\n BEFORE entity;");
		s1.executeUpdate();

		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Accounts" +
			"\n ADD COLUMN IF NOT EXISTS" +
			"\n actorShortId VARCHAR(128)" +
			"\n	DEFAULT NULL" +
			"\n BEFORE actorId;");
		s2.executeUpdate();

		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE AccessTokens" +
			"\n ADD COLUMN IF NOT EXISTS" +
			"\n actorShortId VARCHAR(128)" +
			"\n	DEFAULT NULL" +
			"\n BEFORE actorId;");
		s3.executeUpdate();
	}

	public static void
	fillUsernames(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"SELECT * FROM Actors;");
		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n UPDATE Actors" +
			"\n SET username = ?" +
			"\n WHERE id = ?;");
		ResultSet r2 = s2.executeQuery();
		while (r2.next()) try
		{
			StringReader r = new StringReader(r2.getString(3));
			JsonObject json = Json.createReader(r).readObject();
			String username = json.getString("username", null);
			assert username != null;
			
			s3.setString(1, username);
			s3.setString(2, r2.getString(1));
			s3.executeUpdate();
		}
		catch (JsonParsingException eJp)
		{
			eJp.printStackTrace();
			assert false;
		}
		catch (JsonException eJson)
		{
			eJson.printStackTrace();
			assert false;
		}

		mainDbConn.commit();
	}

	public static void
	changeIdsInAccountsToShortIds(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"SELECT * FROM Accounts;");
		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n UPDATE Accounts" +
			"\n SET actorShortId = ?" +
			"\n WHERE actorId = ?;");
		ResultSet r2 = s2.executeQuery();
		while (r2.next())
		{
			String actorId = r2.getString(2);
			// We have a new column at the front atm.
			assert actorId != null;
			String actorShortId = idToShortId(actorId);
			s3.setString(1, actorShortId);
			s3.setString(2, actorId);
			s3.executeUpdate();
		}

		mainDbConn.commit();
	}

	public static void
	changeIdsInAccessTokensToShortIds(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"SELECT * FROM AccessTokens;");
		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n UPDATE AccessTokens" +
			"\n SET actorShortId = ?" +
			"\n WHERE actorId = ?;");
		ResultSet r2 = s2.executeQuery();
		while (r2.next())
		{
			String actorId = r2.getString(2);
			assert actorId != null;
			String actorShortId = idToShortId(actorId);
			s3.setString(1, actorShortId);
			s3.setString(2, actorId);
			s3.executeUpdate();
		}

		mainDbConn.commit();
	}

	public static void
	finalAlters(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Accounts" +
			"\n DROP COLUMN actorId;");
		s1.executeUpdate();

		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE AccessTokens" +
			"\n DROP COLUMN actorId;");
		s2.executeUpdate();
	}

	public static void
	changeVersion(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s = mainDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 4;");
		s.executeUpdate();

		mainDbConn.commit();
	}

//	 -	-%-	 -

	private static String
	idToShortId(String id)
	{
		String regex = "https://(.+)/(.+)/(.+)";
		Matcher m = Pattern.compile(regex).matcher(id);
		if (!m.matches()) return null;
		return m.group(3);
	}

}