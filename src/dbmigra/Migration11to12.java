
import java.sql.*;
import java.util.regex.*;
import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.time.*;
import java.time.format.DateTimeParseException;
import java.io.*;

class
Migration11to12 {

	public static void
	main(String... args)
	throws SQLException, JsonParsingException, JsonException
	{
		Connection mainDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/main", "Rani", "");
		Connection objectDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/object", "Rani", "");

		mainDbConn.setAutoCommit(false);
		objectDbConn.setAutoCommit(false);

		addColumns(mainDbConn, objectDbConn);
		addValues(mainDbConn, objectDbConn);
		changeVersion(mainDbConn, objectDbConn);

		mainDbConn.close();
		objectDbConn.close();
	}

	public static void
	addColumns(Connection mainDbConn, Connection objectDbConn)
	throws SQLException
	{
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Objects" +
			"\n ADD COLUMN" +
			"\n created TIMESTAMP" +
			"\n BEFORE entity;");
		s2.executeUpdate();

		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Activities" +
			"\n ADD COLUMN" +
			"\n created TIMESTAMP" +
			"\n BEFORE entity;");
		s3.executeUpdate();

		PreparedStatement s4 = objectDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Attachments" +
			"\n ADD COLUMN" +
			"\n created TIMESTAMP" +
			"\n BEFORE entity;");
		s4.executeUpdate();

		mainDbConn.commit();
		objectDbConn.commit();
	}

	public static void
	addValues(Connection mainDbConn, Connection objectDbConn)
	throws SQLException
	{
		extractToCreatedColumn(mainDbConn, "Objects");
		extractToCreatedColumn(mainDbConn, "Activities");
		extractToCreatedColumn(objectDbConn, "Attachments");

		mainDbConn.commit();
		objectDbConn.commit();
	}

	public static void
	changeVersion(Connection mainDbConn, Connection objectDbConn)
	throws SQLException
	{
		PreparedStatement s = mainDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 12;");
		s.executeUpdate();

		s = objectDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 12;");
		s.executeUpdate();

		mainDbConn.commit();
		objectDbConn.commit();
	}

//	 -	-%-	 -

	private static void
	extractToCreatedColumn(Connection dbConn, String tableName)
	throws SQLException
	{
		PreparedStatement s1 = dbConn.prepareStatement(
			"\n SELECT id, entity" +
			"\n FROM " + tableName + ";");
		PreparedStatement s2 = dbConn.prepareStatement(
			"\n UPDATE " + tableName +
			"\n SET created = ?" +
			"\n WHERE id = ?;");

		ResultSet r1 = s1.executeQuery();
		while (r1.next())
		{
			JsonObject entity = toJsonObject(r1.getString(2));
			String c1 = entity.getString("created");
			ZonedDateTime c2 = ZonedDateTime.parse(c1);
			Timestamp c3 = Timestamp.from(c2.toInstant());

			s2.setString(2, r1.getString(1));
			s2.setTimestamp(1, c3);
			s2.executeUpdate();
		}
	}

	private static JsonObject
	toJsonObject(String string)
	throws JsonParsingException, JsonException
	{
		return Json.createReader(new StringReader(string))
			.readObject();
	}

}
