
import java.sql.*;
import java.util.regex.*;
import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.io.*;

class
Migration4to5 {

	public static void
	main(String... args)
	throws SQLException
	{
		Connection mainDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/main", "Rani", "");
		Connection objectDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/object", "Rani", "");

		mainDbConn.setAutoCommit(false);
		objectDbConn.setAutoCommit(false);

		amendActors(mainDbConn);
		changeVersion(mainDbConn);

		mainDbConn.close();
		objectDbConn.close();
	}

	public static void
	amendActors(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"SELECT * FROM Actors;");
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n UPDATE Actors" +
			"\n SET entity = ?" +
			"\n WHERE username = ?;");
		ResultSet r1 = s1.executeQuery();
		while (r1.next()) try
		{
			StringReader r = new StringReader(r1.getString(3));
			JsonObject actorJson = Json.createReader(r).readObject();

			String id = actorJson.getString("id", null);
			String username = actorJson.getString("username", null);
			assert id != null;
			assert username != null;

			actorJson = ActivityPubUtil.edit(actorJson)
				.add("@context", ActivityPubUtil.basicContext)
				.add("type", "Person")
				.add("name", username)
				.add("inbox", id + "/inbox")
				.add("outbox", id + "/outbox")
				.build();
			
			s2.setString(1, actorJson.toString());
			s2.setString(2, username);
			s2.executeUpdate();
		}
		catch (JsonParsingException eJp)
		{
			eJp.printStackTrace();
			assert false;
		}
		catch (JsonException eJson)
		{
			eJson.printStackTrace();
			assert false;
		}

		mainDbConn.commit();
	}

	public static void
	changeVersion(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s = mainDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 5;");
		s.executeUpdate();

		mainDbConn.commit();
	}

//	 -	-%-	 -

	private static String
	idToShortId(String id)
	{
		String regex = "https://(.+)/(.+)/(.+)";
		Matcher m = Pattern.compile(regex).matcher(id);
		if (!m.matches()) return null;
		return m.group(3);
	}

}