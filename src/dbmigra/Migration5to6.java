
import java.sql.*;
import java.util.regex.*;
import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.io.*;

class
Migration5to6 {

	public static void
	main(String... args)
	throws SQLException
	{
		Connection mainDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/main", "Rani", "");
		Connection objectDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/object", "Rani", "");

		mainDbConn.setAutoCommit(false);
		objectDbConn.setAutoCommit(false);

		initialAlters(mainDbConn);
		fillActorsValues(mainDbConn);
		fillObjectsValues(mainDbConn);
		fillActivitiesValues(mainDbConn);
		changeVersion(mainDbConn);

		mainDbConn.close();
		objectDbConn.close();
	}

	public static void
	initialAlters(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Actors" +
			"\n ADD COLUMN IF NOT EXISTS" +
			"\n shortId VARCHAR(128)" +
			"\n DEFAULT NULL" +
			"\n BEFORE username;");
		s1.executeUpdate();

		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Objects" +
			"\n ADD COLUMN IF NOT EXISTS" +
			"\n shortId VARCHAR(128)" +
			"\n DEFAULT NULL" +
			"\n BEFORE entity;");
		s2.executeUpdate();

		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE Activities" +
			"\n ADD COLUMN IF NOT EXISTS" +
			"\n shortId VARCHAR(128)" +
			"\n DEFAULT NULL" +
			"\n BEFORE entity;");
		s3.executeUpdate();

		mainDbConn.commit();
	}

	public static void
	fillActorsValues(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"SELECT * FROM Actors;");
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n UPDATE Actors" +
			"\n SET shortId = ?" +
			"\n WHERE id = ?;");
		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n UPDATE Actors" +
			"\n SET id = ?" +
			"\n WHERE id = ?;");
		s3.setNull(1, Types.VARCHAR);

		ResultSet r1 = s1.executeQuery();
		while (r1.next())
		{
			String id = r1.getString(1);
			String shortId = ActivityPubUtil.localIdToShortId(id);
			s2.setString(1, shortId);
			s2.setString(2, id);
			s2.executeUpdate();
			s3.setString(2, id);
			s3.executeUpdate();
		}

		mainDbConn.commit();
	}

	public static void
	fillObjectsValues(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"SELECT * FROM Objects;");
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n UPDATE Objects" +
			"\n SET shortId = ?" +
			"\n WHERE id = ?;");
		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n UPDATE Objects" +
			"\n SET id = ?" +
			"\n WHERE id = ?;");
		s3.setNull(1, Types.VARCHAR);

		ResultSet r1 = s1.executeQuery();
		while (r1.next())
		{
			String id = r1.getString(1);
			String shortId = ActivityPubUtil.localIdToShortId(id);
			s2.setString(1, shortId);
			s2.setString(2, id);
			s2.executeUpdate();
			s3.setString(2, id);
			s3.executeUpdate();
		}

		mainDbConn.commit();
	}

	public static void
	fillActivitiesValues(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"SELECT * FROM Activities;");
		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n UPDATE Activities" +
			"\n SET shortId = ?" +
			"\n WHERE id = ?;");
		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n UPDATE Activities" +
			"\n SET id = ?" +
			"\n WHERE id = ?;");
		s3.setNull(1, Types.VARCHAR);

		ResultSet r1 = s1.executeQuery();
		while (r1.next()) 
		{
			String id = r1.getString(1);
			String shortId = ActivityPubUtil.localIdToShortId(id);
			s2.setString(1, shortId);
			s2.setString(2, id);
			s2.executeUpdate();
			s3.setString(2, id);
			s3.executeUpdate();
		}

		mainDbConn.commit();
	}

	public static void
	changeVersion(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s = mainDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 6;");
		s.executeUpdate();

		mainDbConn.commit();
	}

//	 -	-%-	 -

	private static String
	idToShortId(String id)
	{
		String regex = "https://(.+)/(.+)/(.+)";
		Matcher m = Pattern.compile(regex).matcher(id);
		if (!m.matches()) return null;
		return m.group(3);
	}

}