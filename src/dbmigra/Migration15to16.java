

import java.sql.*;
import java.util.regex.*;
import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.time.*;
import java.time.format.DateTimeParseException;
import java.io.*;
import java.util.*;

class
Migration15to16 {

	public static void
	main(String... args)
	throws SQLException, JsonParsingException, JsonException
	{
		Connection mainDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/main", "Rani", "");
		Connection objectDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/object", "Rani", "");

		mainDbConn.setAutoCommit(false);
		objectDbConn.setAutoCommit(false);

		Map<String, ZonedDateTime> expiryDates =
			removeExpiryDates(mainDbConn);
		/*
		* HSQLDB's built-in converter cannot cope with
		* ZonedDateTime's serialisation towards Timestamp,
		* so we will do it manually in Java.
		*/
		alterTables(mainDbConn);
		reinsertExpiryDates(expiryDates, mainDbConn);
		changeVersion(mainDbConn, objectDbConn);

		mainDbConn.close();
		objectDbConn.close();
	}

	public static Map<String, ZonedDateTime>
	removeExpiryDates(Connection mainDbConn)
	throws SQLException
	{
		Map<String, ZonedDateTime> returnee = new HashMap<>();

		PreparedStatement s1 = mainDbConn.prepareStatement(
			"SELECT * FROM AccessTokens;");
		ResultSet r1 = s1.executeQuery();
		while (r1.next()) try
		{
			String token = r1.getString(2);
			ZonedDateTime expiryDate = ZonedDateTime
				.parse(r1.getString(3));
			returnee.put(token, expiryDate);
		}
		catch (DateTimeParseException eDtp)
		{
			assert false;
		}

		PreparedStatement s2 = mainDbConn.prepareStatement(
			"UPDATE AccessTokens SET expiryDate = ?;");
		s2.setNull(1, Types.TIMESTAMP);
		s2.executeUpdate();

		mainDbConn.commit();
		return returnee;
	}

	public static void
	alterTables(Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE JobLog" +
			"\n ALTER" +
			"\n COLUMN startDate TIMESTAMP;");
		s1.executeUpdate();

		PreparedStatement s2 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE JobLog" +
			"\n ALTER" +
			"\n COLUMN endDate TIMESTAMP;");
		s2.executeUpdate();

		PreparedStatement s3 = mainDbConn.prepareStatement(
			"\n ALTER" +
			"\n TABLE AccessTokens" +
			"\n ALTER" +
			"\n COLUMN expiryDate TIMESTAMP;");
		s3.executeUpdate();

		mainDbConn.commit();
	}

	public static void
	reinsertExpiryDates(
		Map<String, ZonedDateTime> expiryDates,
		Connection mainDbConn)
	throws SQLException
	{
		PreparedStatement s = mainDbConn.prepareStatement(
			"\n UPDATE AccessTokens" +
			"\n SET expiryDate = ?" +
			"\n WHERE token = ?;");

		for (String token: expiryDates.keySet())
		{
			Timestamp tExpiryDate = Timestamp
				.from(expiryDates.get(token).toInstant());
			s.setTimestamp(1, tExpiryDate);
			s.setString(2, token);
			s.executeUpdate();
		}

		mainDbConn.commit();
	}

	public static void
	changeVersion(Connection mainDbConn, Connection objectDbConn)
	throws SQLException
	{
		PreparedStatement s = mainDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 16;");
		s.executeUpdate();

		s = objectDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 16;");
		s.executeUpdate();

		mainDbConn.commit();
		objectDbConn.commit();
	}

}