
import java.sql.*;
import java.util.regex.*;
import javax.json.*;
import javax.json.stream.JsonParsingException;
import java.time.*;
import java.time.format.DateTimeParseException;
import java.io.*;

class
Migration14to15 {

	public static void
	main(String... args)
	throws SQLException, JsonParsingException, JsonException
	{
		Connection mainDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/main", "Rani", "");
		Connection objectDbConn = DriverManager.getConnection(
			"jdbc:hsqldb:file:db/object", "Rani", "");

		mainDbConn.setAutoCommit(false);
		objectDbConn.setAutoCommit(false);

		addTables(mainDbConn, objectDbConn);
		changeVersion(mainDbConn, objectDbConn);

		mainDbConn.close();
		objectDbConn.close();
	}

	public static void
	addTables(Connection mainDbConn, Connection objectDbConn)
	throws SQLException
	{
		PreparedStatement s1 = mainDbConn.prepareStatement(
			"\n CREATE" +
			"\n TABLE JobLog" +
			"\n (id VARCHAR(128)," +
			"\n status VARCHAR(64)," +
			"\n startDate VARCHAR(128)," +
			"\n endDate VARCHAR(128)," +
			"\n summary VARCHAR(256));");
		s1.executeUpdate();

		mainDbConn.commit();
		objectDbConn.commit();
	}

	public static void
	changeVersion(Connection mainDbConn, Connection objectDbConn)
	throws SQLException
	{
		PreparedStatement s = mainDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 15;");
		s.executeUpdate();

		s = objectDbConn.prepareStatement(
			"\n UPDATE ServerInfo" +
			"\n SET DatabaseVersion = 15;");
		s.executeUpdate();

		mainDbConn.commit();
		objectDbConn.commit();
	}

}