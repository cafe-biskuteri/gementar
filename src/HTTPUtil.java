
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.io.Reader;
import java.io.Writer;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

class
HTTPRequest {

	String
	method;

	URI
	uri;

	Map<String, String>
	params = new HashMap<>();

	Map<String, String>
	headers = new HashMap<>();

	String
	body;

}

class
HTTPResponse {

	String
	method;

	URI
	uri;

	int
	code = 500;

	Map<String, String>
	headers = new HashMap<>();

	String
	body;

}

class
HTTPRequestReader {

	public static void
	read(HTTPRequest returnee, Reader r)
	throws IOException
	{
		assert returnee != null;
		assert r != null;

		StringBuilder b = new StringBuilder();
		String pattern = "\r\n"; int mo = 0;
		boolean inBody = false;
		while (true)
		{
			int c = r.read();
			if (c != -1) b.append((char)c);

			if (c == pattern.charAt(mo)) ++mo;
			if (c != -1 && mo < pattern.length()) continue;

			int n = b.length() - pattern.length();
			String line = b.substring(0, n);
			
			if (returnee.method == null)
			{
				if (!validRequestLine(line)) break;
				acceptRequestLine(returnee, line);
			}
			else if (line.isEmpty())
			{
				final String CL = "Content-Length";
				if (!returnee.headers.containsKey(CL)) break;
				/*
				* Technically, we could've also unconditionally
				* broke at end of header section, and let caller
				* decide whether to recall us/call #readBody.
				* That would've also allowed us to continue
				* simple partitioning of lines rather than full
				* parsing that we do here. But, whatever.
				*/

				try
				{
					String cl = returnee.headers.get(CL);
					n = Integer.parseInt(cl);
					char[] buf = new char[n];
					n = r.read(buf, 0, buf.length);
					returnee.body = new String(buf, 0, n);
					break;
				}
				catch (NumberFormatException eNf)
				{
					break;
				}
			}
			else
			{
				if (!validHeaderLine(line)) break;
				acceptHeaderLine(returnee, line);
			}

			mo = 0;
			b.setLength(0);
			if (c == -1) break;
		}
	}

//	 -	-%-	 -

	public static boolean
	validRequestLine(String line)
	{
		String[] fields = line.split(" ");

		if (fields.length != 3) return false;

		if (!fields[2].equals("HTTP/1.1")) return false;

		try { new URI(fields[1]); }
		catch (URISyntaxException eUri) { return false; }

		return true;
	}

	public static void
	acceptRequestLine(HTTPRequest request, String line)
	{
		String[] fields = line.split(" ");

		request.method = fields[0];

		try { request.uri = new URI(fields[1]); }
		catch (URISyntaxException eUri) { assert false; }

		String query = request.uri.getQuery();
		if (query != null)
		for (String param: query.split("&"))
		{
			int o = param.indexOf("=");
			String key = param.substring(0, o);
			String value = param.substring(o + 1);
			request.params.put(key, value);
		}
	}

	public static boolean
	validHeaderLine(String line)
	{
		int o = line.indexOf(":");
		if (o == -1) return false;

		String name = line.substring(0, o);
		if (name.contains(" ")) return false;
		if (name.isEmpty()) return false;

		return true;
	}

	public static void
	acceptHeaderLine(HTTPRequest request, String line)
	{
		int o = line.indexOf(":");
		String name = line.substring(0, o);
		String value = line.substring(o + 1);
		for (o = 0; o < value.length(); ++o)
		{
			char c = value.charAt(o);
			if (Character.isWhitespace(c)) continue;
			value = value.substring(o);
			break;
		}
		request.headers.put(name, value);
	}

	/*
	public static void
	acceptBodyLine(HTTPRequest request, String line)
	{
		if (request.body == null) request.body = "";
		request.body += line + "\r\n";
	}
	*/

}

class
HTTPResponseWriter {

	public static void
	write(HTTPResponse response, Writer w)
	throws IOException
	{
		w.write(toString(response));
		w.flush();
	}

	public static String
	toString(HTTPResponse response)
	{
		StringBuilder b = new StringBuilder();

		b.append("HTTP/1.1 " + response.code + "\r\n");

		Set<String> names = response.headers.keySet();
		List<String> sortedNames = new ArrayList<>(names);
		Collections.sort(sortedNames);
		for (String name: sortedNames)
		{
			String value = response.headers.get(name);
			b.append(name + ": " + value + "\r\n");
		}
		b.append("\r\n");

		if (response.body != null) b.append(response.body);

		return b.toString();
	}

}

class
HTTPUtil {

	public static boolean
	validHTTPRequest(HTTPRequest request)
	{
		String m = request.method;
		if (m == null) return false;
		if (!m.equals(m.trim())) return false;
		if (!m.equals(m.toUpperCase())) return false;

		if (request.uri == null) return false;

		for (String name: request.headers.keySet())
		{
			if (name == null) return false;
			if (name.contains(" ")) return false;
			if (name.isEmpty()) return false;
		}

		if (request.body != null)
		{
			int n = request.body.getBytes().length;
			String cl = request.headers.get("Content-Length");
			if (cl == null) return false;
			if (!cl.equals("" + n)) return false;
		}

		return true;
	}

	public static boolean
	validHTTPResponse(HTTPResponse response)
	{
		//if (response.method == null) return false;
		//if (response.uri == null) return false;

		for (String name: response.headers.keySet())
		{
			if (name.contains(" ")) return false;
			if (name.isEmpty()) return false;
		}

		return true;
	}

}