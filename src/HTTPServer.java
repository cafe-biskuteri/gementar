
import java.net.ServerSocket;
import java.net.Socket;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.Collections;
import java.util.Base64;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Signature;
import java.security.PrivateKey;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.InvalidKeyException;

class
HTTPServer {

	private Logger
	logger;

	private Set<Route>
	routes;

//	 -	-%-	 -

	private ServerSocket
	serverSocket;

	private ExecutorService
	executor;

//	---%-@-%---

	public void
	listenLoop()
	{
		int port = serverSocket.getLocalPort();
		logger.log("Listening on " + port + ".");

		while (true) try
		{
			Socket socket = serverSocket.accept();
			String addr = socket.getInetAddress().toString();
			logger.log("CONN " + addr + " connected");
			executor.execute(new ServeTask(logger, socket, routes));
		}
		catch (IOException eIo)
		{
			String msg = "Couldn't accept client socket";
			logger.log(new IOException(msg, eIo));
			logger.log("Server's listener loop shutting down!");
			break;
		}
	}

//	---%-@-%---

	HTTPServer(int port, Logger logger)
	throws IOException
	{
		this.logger = logger;

		serverSocket = new ServerSocket(port);

		executor = Executors.newCachedThreadPool();

		routes = Collections.emptySet();
	}

	public void
	setRoutes(Set<Route> routes)
	{
		assert routes != null;
		this.routes = routes;
	}

}



interface
Route {

	String
	getMethod();

	String
	getPath();

	HTTPResponse
	invoke(HTTPRequest request)
	throws Throwable;

}



class
ServeTask implements Runnable {

	Logger
	logger;

	Socket
	socket;

	Set<Route>
	routes;

	Map<String, KeyPair>
	keystore;

//	 -	-%-	 -

	private HTTPRequest
	request;

	private HTTPResponse
	response;

//	---%-@-%---

	public void
	run()
	{
		assert socket.isConnected();

		request = new HTTPRequest();
		response = null;
		readValidHTTPRequest();

		if (response == null && !socket.isClosed())
			respondToRequest();
		
		writeValidHTTPResponse();
	}

//	 -	-%-	 -

	private void
	readValidHTTPRequest()
	{
		try
		{
			InputStream in = socket.getInputStream();
			InputStreamReader r = new InputStreamReader(in);

			HTTPRequestReader.read(request, r);

			if (!HTTPUtil.validHTTPRequest(request))
				response = invalidHTTPRequestResponse();
		}
		catch (IOException eIo)
		{
			logger.log("Couldn't read from socket", eIo);
			closeSocket(socket);
		}
	}

	private void
	respondToRequest()
	{
		try
		{
			response = route(request, routes);
			signResponse(keystore, response);
		}
		catch (Throwable e)
		{
			assert !(e instanceof InvalidKeyException);
			assert !(e instanceof SignatureException);
			/*
			* Why would these be impossible? We will soon be using
			* the private key of the actor. If for some reason a
			* valid key isn't available from the database, then
			* exceptions can occur.
			*/

			logger.log("Uncaught throwable from route:", e);
			response = genericErrorResponse(e);
		}
	}

	private void
	writeValidHTTPResponse()
	{
		assert response != null;
		assert HTTPUtil.validHTTPResponse(response);

		if (response.body != null) response.body += "\r\n";

		try
		{
			OutputStream out = socket.getOutputStream();
			OutputStreamWriter w = new OutputStreamWriter(out);
			HTTPResponseWriter.write(response, w);
			closeSocket(socket);
		}
		catch (IOException eIo)
		{
			logger.log("Couldn't write to socket", eIo);
			closeSocket(socket);
		}
	}

//	 -	-%-	 -

	private static void
	closeSocket(Socket socket)
	{
		try { socket.close(); }
		catch (IOException eIo) { assert socket.isClosed(); }
	}


	private static HTTPResponse
	route(HTTPRequest request, Set<Route> routes)
	throws Throwable
	{
		for (Route route: routes)
		{
			if (!matchesRoute(request, route)) continue;

			addPathParams(request, route.getPath());
			HTTPResponse response = route.invoke(request);
			removePathParams(request);

			return response;
		}

		return noRouteResponse();
	}

	private static boolean
	matchesRoute(HTTPRequest request, Route route)
	{
		return matchesRoute(
			request, route.getMethod(), route.getPath());
	}

	public static boolean
	matchesRoute(
		HTTPRequest request, String routeMethod, String routePath)
	{
		if (!routeMethod.equals(request.method)) return false;

		assert !routePath.contains("(");
		Pattern pattern = Pattern.compile(
			routePath.replaceAll("<>", "([^/]+)")+ "/?");
		Matcher matcher = pattern.matcher(request.uri.getPath());
		if (!matcher.matches()) return false;

		return true;
	}

	public static void
	addPathParams(HTTPRequest request, String routePath)
	{
		Pattern pattern = Pattern.compile(
			routePath.replaceAll("<>", "([^/]+)")+ "/?");
		Matcher matcher = pattern.matcher(request.uri.getPath());
		boolean matched = matcher.matches();
		assert matched;

		for (int i = 1; i <= matcher.groupCount(); ++i)
		{
			String value = matcher.group(i);
			String key = "\b" + i;
			request.params.put(key, value);
		}
	}

	private static void
	removePathParams(HTTPRequest request)
	{
		for (String key: request.params.keySet())
		{
			if (key.startsWith("\b"))
				request.params.remove(key);
		}
	}


	private static void
	signResponse(
		Map<String, KeyPair> keystore, HTTPResponse response)
	throws InvalidKeyException, SignatureException
	{
		KeyPair cred = keystore.get("Gemetar-dev");
		/*
		* We should get a key for the server itself, or maybe
		* the actor for which the response was intended.
		* Right now we use an in-memory keystore initialised
		* in our constructor, but the private keys of actors
		* in the database, are they also SHA256withRSA?
		*/
		signResponse(cred.getPrivate(), response);
	}

	private static void
	signResponse(PrivateKey key, HTTPResponse response)
	throws InvalidKeyException, SignatureException
	{
		if (response.method == null) return;
		if (response.uri == null) return;

		Signature sign = signature("SHA256withRSA");
		sign.initSign(key);

		String date =
			DateTimeFormatter.RFC_1123_DATE_TIME
				.format(ZonedDateTime.now(ZoneOffset.UTC));
		StringBuilder b = new StringBuilder();
		b.append("(request-target): ");
		b.append(response.method.toLowerCase() + " ");
		b.append(response.uri.toString().toLowerCase() + "\n");
		b.append("host: localhost\n");
		b.append("date: " + date);
		byte[] data = b.toString().getBytes();
		sign.update(data);
		b.setLength(0);

		String signature =
			Base64.getEncoder().encodeToString(sign.sign());
		b.append("keyId=\"Gemetar-dev\"");
		b.append(",headers=\"(request-target) host date\"");
		b.append(",signature=\"" + signature + "\"");
		String value = b.toString();
		b.setLength(0);

		response.headers.put("Signature", value);
	}


	private static HTTPResponse
	invalidHTTPRequestResponse()
	{
		HTTPResponse returnee = new HTTPResponse();
		returnee.code = 400;
		returnee.body = "This server serves only HTTP requests..";
		return returnee;
	}

	private static HTTPResponse
	noRouteResponse()
	{
		HTTPResponse returnee = new HTTPResponse();
		returnee.code = 400;
		returnee.body =
			"We do not recognise the endpoint you're trying to"
			+ " call. Check your HTTP method? That forms part"
			+ " of our recognition.";
		return returnee;
	}

	private static HTTPResponse
	genericErrorResponse(Throwable t)
	{
		HTTPResponse returnee = new HTTPResponse();
		returnee.code = 500;
		returnee.body =
			"We ran into a problem internally, and can't"
			+ " serve your request. Apologies..!";
		return returnee;
	}

//	---%-@-%---

	public
	ServeTask(Logger logger, Socket socket, Set<Route> routes)
	{
		this.logger = logger;
		this.socket = socket;
		this.routes = routes;

		keystore = new HashMap<>();
		keystore.put(
			"Gemetar-dev",
			keyPairGen("RSA").generateKeyPair());
	}

//	 -	-%-	 -

	private static KeyPairGenerator
	keyPairGen(String algorithm)
	{
		try { return KeyPairGenerator.getInstance(algorithm); }
		catch (NoSuchAlgorithmException eNsa) { assert false; }
		return null;
	}

	private static Signature
	signature(String algorithm)
	{
		try { return Signature.getInstance(algorithm); }
		catch (NoSuchAlgorithmException eNsa) { assert false; }
		return null;
	}

}
