
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.io.IOException;
import java.sql.SQLException;
import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.stream.JsonParsingException;
import java.io.StringReader;

class
Gemetar {

	public static boolean
	enforceRouteRequirements(
		GemetarRouteRequirements req,
		HTTPRequest request,
		HTTPResponse response)
	throws SQLException
	{
		if (!enforceAuth(req, request, response)) return false;

		if (!enforceContentType(req, request, response))
			return false;

		if (!enforceQueryParams(req, request, response))
			return false;

		if (!enforceBodyParams(req, request, response))
			return false;

		return true;
	}

//	 -	-%-	 -

	private static boolean
	enforceContentType(
		GemetarRouteRequirements req,
		HTTPRequest request,
		HTTPResponse response)
	{
		if (req.requiredContentType == null) return true;

		String ct = request.headers.get("Content-Type");
		if (ct == null) return false;
		if (!ct.equals(req.requiredContentType)) return false;
		if (request.body == null) return false;

		return true;
	}

	private static boolean
	enforceQueryParams(
		GemetarRouteRequirements req,
		HTTPRequest request,
		HTTPResponse response)
	{
		if (req.requiredQueryParams == null) return true;

		for (String param: req.requiredQueryParams)
		if (!request.params.containsKey(param))
		{
			response.code = 400;
			response.body = "'" + param + "' param needed.";
			return false;
		}

		return true;
	}

	private static boolean
	enforceBodyParams(
		GemetarRouteRequirements req,
		HTTPRequest request,
		HTTPResponse response)
	{
		if (req.requiredBodyParams == null) return true;

		req.bodyParams = getJsonBody(req, request, response);
		if (req.bodyParams == null) return false;

		for (String key: req.requiredBodyParams.keySet())
		{
			JsonValue value = req.bodyParams.get(key);
			Class type = req.requiredBodyParams.get(key);

			if (value == null)
			{
				response.code = 400;
				response.body = "Param '" + key + "' is missing.";
				return false;
			}
			if (!type.isInstance(value))
			{
				response.code = 400;
				response.body =
					"Param '" + key + "' does not " +
					"have value of correct JSON type.";
				return false;
			}
		}
		return true;
	}

	private static boolean
	enforceAuth(
		GemetarRouteRequirements req,
		HTTPRequest request,
		HTTPResponse response)
	throws SQLException
	{
		if (!req.authRequired) return true;

		String accessToken = getAccessToken(request);
		if (accessToken == null)
		{
			response.code = 401;
			response.body =
				"Access token, provided as Base64 in the " +
				"Authorisation request header, required.";
			return false;
		}

		JsonObject tokenOwnerJson = req.dataAPI
			.getAccessTokenOwner(accessToken);
		if (tokenOwnerJson == null)
		{
			response.code = 401;
			response.body = "No account owns that access token.";
			return false;
		}

		String tokenOwnerUsername = tokenOwnerJson
			.getString("username");
		if (req.requiredUsername != null)
		if (!tokenOwnerUsername.equals(req.requiredUsername))
		{
			response.code = 401;
			response.body =
				"Access token does not match any " +
				"owner of resource.";
			return false;
		}

		req.userEntity = tokenOwnerJson;
		return true;
	}

	private static JsonObject
	getJsonBody(
		GemetarRouteRequirements req,
		HTTPRequest request,
		HTTPResponse response)
	{
		if (request.body == null)
		{
			response.code = 400;
			response.body = "Request body is not valid JSON.";
			return null;
		}
		else try
		{
			StringReader r = new StringReader(request.body);
			return Json.createReader(r).readObject();
		}
		catch (JsonParsingException eJp)
		{
			response.code = 400;
			response.body = "Request body is not valid JSON.";
			return null;
		}
		catch (JsonException eJson)
		{
			response.code = 500;
			response.body = "Error in server while parsing JSON.";
			return null;
		}
	}

//	 -	-%-	 -

	private static String
	getAccessToken(HTTPRequest request)
	{
		String name = null;
		if (request.headers.containsKey("Authorization"))
			name = "Authorization";
		if (request.headers.containsKey("Authorisation"))
			name = "Authorisation";
		if (name == null) return null;

		String value = request.headers.get(name);
		assert value != null;
		if (!value.startsWith("Bearer ")) return null;

		return value.substring("Bearer ".length());
	}

//	---%-@-%---

	public static void
	main(String... args)
	throws SQLException, IOException
	{
		ConfigAPI c = new ConfigAPI();

		DataAPI d; try
		{
			String profileName = findArg(args, "--profile");

			ConfigAPI.DbConnProfile dbConnProfile = c
				.readDbConnProfile(profileName);

			d = new DataAPI(dbConnProfile);
		}
		catch (ConfigAPI.ConfigException eCon)
		{
			eCon.printStackTrace();
			return;
		}

		NetworkAPI n = new NetworkAPI(d);
		JobAPI j = new JobAPI(d);
		Logger logger = new Logger();

		Set<Route> routes = new HashSet<>();
		routes.add(new WebfingerRoute(d, logger));
		routes.add(new GetLocalActorRoute(d, logger));
		routes.add(new GetLocalObjectRoute(d, logger));
		routes.add(new GetLocalActivityRoute(d, logger));
		routes.add(new RegisterRoute(d, logger));
		routes.add(new AuthenticateRoute(d, logger));
		routes.add(new CreateNoteRoute(d, n, j, logger));
		routes.add(new ReadInboxRoute(d, logger));
		routes.add(new ReadFederatedRoute(d, logger));

		HTTPServer instance = new HTTPServer(8080, logger);
		instance.setRoutes(routes);
		instance.listenLoop();
	}

//	 -	-%-	 -

	public static String
	findArg(String[] args, String longArg)
	{
		assert longArg != null;
		assert longArg.startsWith("--");
		assert longArg.length() > 2;

		String shortArg = "-" + longArg.charAt(2);

		for (int o = 0; o < args.length; ++o)
		{
			boolean isLongArg = args[o].equals(longArg);
			boolean isShortArg = args[o].equals(shortArg);
			if (!(isLongArg || isShortArg)) continue;

			String value = "", nextField = "-";
			if (o < args.length - 1) nextField = args[o + 1];
			if (!nextField.startsWith("-")) value = nextField;

			return value;
		}

		return null;
	}
	
}

class
GemetarRouteRequirements {

	String[]
	requiredQueryParams = null;

	Map<String, Class>
	requiredBodyParams = null;

	boolean
	authRequired = false;

	String
	requiredUsername = null;

	String
	requiredContentType = null;

	DataAPI
	dataAPI = null;

	JsonObject
	userEntity;

	JsonObject
	bodyParams;

}
