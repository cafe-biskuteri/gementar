
import javax.json.JsonObject;
import javax.json.JsonArray;
import javax.json.JsonString;
import javax.json.JsonValue;
import java.util.Collection;
import java.util.Set;
import java.util.HashSet;
import java.sql.SQLException;

class
NetworkAPI {

	private DataAPI
	dataAPI;

//	---%-@-%---

	public void
	deliverActivity(JsonObject activity)
	throws SQLException
	{
		assert activity != null;

		String creatorId = activity.getString("actor");
		JsonObject creator = dataAPI.getActor(creatorId);
		if (creator != null)
		{
			assert Entities.validActor(creator);
			String username = creator.getString("username", null);
			dataAPI.saveInInbox(username, activity);
		}

		Audience audience = parseAudience(activity);
		JsonObject filteredActivity = Entities
			.filterPrivateFromActivity(activity);

		// This software doesn't have an instance-local shared
		// inbox yet.
		
		for (String target: audience.to)
			deliverActivityRemote(target, filteredActivity);

		for (String target: audience.cc)
			deliverActivityRemote(target, filteredActivity);
	}

//	 -	-%-	 -

	public static Audience
	parseAudience(JsonObject activity)
	{
		Set<String> to = toStringSet(activity.getJsonArray("to"));
		Set<String> cc = toStringSet(activity.getJsonArray("cc"));
		// Remove from cc those already in to.
		cc.removeAll(to);

		// Detect Public meta target.
		boolean isPublic = to.contains("as:Public");

		// (未) Remove invalid targets.

		Audience returnee = new Audience();
		returnee.to = to;
		returnee.cc = cc;
		returnee.isPublic = isPublic;
		return returnee;
	}

//	 -	-%-	 -

	private void
	deliverActivityRemote(String target, JsonObject activity)
	{
		// (未) Network request
	}

//	 -	-%-	 -

	private static Set<String>
	toStringSet(JsonArray jsonArray)
	{
		if (jsonArray == null) return new HashSet<>();

		Set<String> returnee = new HashSet<>();
		for (JsonValue value: jsonArray)
		{
			if (!(value instanceof JsonString)) continue;
			returnee.add(((JsonString)value).getString());
		}
		return returnee;
	}

//	---%-@-%---

	public static class
	Audience {

		Collection<String>
		to, cc;

		boolean
		isPublic;

	}

//	---%-@-%---

	NetworkAPI(DataAPI dataAPI)
	{
		this.dataAPI = dataAPI;
	}

}
