
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonArray;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.nio.file.Files;

class
TestDataUtil {

	public static void
	fillWithTestMainData(DataAPI dataAPI)
	throws SQLException, IOException
	{
		fillActors(dataAPI);
		fillAccounts(dataAPI);
		fillObjects(dataAPI);
		fillActivities(dataAPI);
	}

	public static void
	fillWithTestObjectData(DataAPI dataAPI)
	{
	
	}

//	 -	-%-	 -

	private static void
	fillActors(DataAPI dataAPI)
	throws SQLException, IOException
	{
		int n = 0;
		for (String line: linesFrom("db/testActors.dsv"))
		{
			String[] fields = line.split(":");
			assert fields.length == 2;
			String username = fields[0];

			JsonObject newActorEntity = Entities
				.generateActor(Json.createObjectBuilder()
					.add("username", username)
					.build());

			dataAPI.saveActor(newActorEntity);
			++n;
		}

		System.err.print("Filled Actors table");
		System.err.println(" with " + n + " actors.");
	}

	private static void
	fillAccounts(DataAPI dataAPI)
	throws SQLException, IOException
	{
		int seed = (int)(Math.random() * Integer.MAX_VALUE);
		int n = 0;
		for (String line: linesFrom("db/testActors.dsv"))
		{
			String[] fields = line.split(":");
			assert fields.length == 2;
			String username = fields[0];
			String passphrase = fields[1];

			if (passphrase.equals("-"))
				passphrase = seed + username;

			dataAPI.createAccount(username, passphrase);
			++n;
		}

		System.err.print("Filled Accounts table");
		System.err.println(" with " + n + " accounts.");
	}

	private static void
	fillObjects(DataAPI dataAPI)
	throws SQLException, IOException
	{
		int n = 0;
		for (String line: linesFrom("db/testObjects.dsv"))
		{
			String[] fields = line.split(":");
			// Since when does String#split clear trailing?
			assert fields.length == 3;
			String shortId = fields[0];
			String type = fields[1];
			String content = fields[2];

			JsonObject newObjectEntity = Entities
				.generateObject(Json.createObjectBuilder()
					.add("shortId", shortId)
					.add("type", type)
					.add("content", content)
					.build());

			dataAPI.saveObject(newObjectEntity);
			++n;
		}

		System.err.print("Filled Objects table");
		System.err.println(" with " + n + " objects.");
	}

	private static void
	fillActivities(DataAPI dataAPI)
	throws SQLException, IOException
	{
		int n = 0;
		for (String line: linesFrom("db/testActivities.dsv"))
		{
			String[] fields = line.split(" ");
			assert fields.length == 5;
			String shortId = fields[0];
			String type = fields[1];
			String actorUsername = fields[2];
			String objectShortId = fields[3];
			String created = fields[4];

			String actorId = ActivityPubUtil.localShortIdToId(
				"actor", actorUsername);
			String objectId = ActivityPubUtil.localShortIdToId(
				"object", objectShortId);

			JsonObject newActivityEntity = Entities
				.generateActivity(Json.createObjectBuilder()
					.add("shortId", shortId)
					.add("type", type)
					.add("actor", actorId)
					.add("object", objectId)
					.add("created", created)
					.add("propagation", "Public")
					.add("targets", Json
						.createArrayBuilder().build())
					.build());

			dataAPI.saveActivity(newActivityEntity);
			dataAPI.saveInOutbox(actorUsername, newActivityEntity);
			dataAPI.saveInInbox(actorUsername, newActivityEntity);
			++n;
		}

		System.err.print("Filled Activities table");
		System.err.println(" with " + n + " activities.");
	}

	private static List<String>
	linesFrom(String path)
	throws IOException
	{
		return Files.readAllLines(new File(path).toPath());
	}

}
