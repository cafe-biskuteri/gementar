
import java.io.StringReader;
import java.io.IOException;

class
HTTPServerTests {

	public static void
	main(String... args)
	throws IOException
	{
		addPathParamsBasicFunctionality();
		matchesRouteBasicFunctionality();
	}

	public static void
	addPathParamsBasicFunctionality()
	throws IOException
	{
		String s = "GET /somewhere HTTP/1.1\r\n\r\n";
		HTTPRequest r = new HTTPRequest();
		HTTPRequestReader.read(r, new StringReader(s));
		assert HTTPUtil.validHTTPRequest(r);

		ServeTask.addPathParams(r, "/<>");
		assert r.params.size() == 1;
		assert r.params.get("\b1").equals("somewhere");

		s = "GET /one/level HTTP/1.1\r\n\r\n";
		r = new HTTPRequest();
		HTTPRequestReader.read(r, new StringReader(s));
		assert r.uri.getPath().equals("/one/level");

		ServeTask.addPathParams(r, "/one/<>");
		assert r.params.size() == 1;
		assert r.params.get("\b1").equals("level");
		r.params.clear();

		ServeTask.addPathParams(r, "/<>/<>");
		assert r.params.size() == 2;
		assert r.params.get("\b1").equals("one");
		assert r.params.get("\b2").equals("level");
	}

	public static void
	matchesRouteBasicFunctionality()
	throws IOException
	{
		String s = "GET /some/1/fancy HTTP/1.1\r\n\r\n";
		HTTPRequest r = new HTTPRequest();
		HTTPRequestReader.read(r, new StringReader(s));
		assert HTTPUtil.validHTTPRequest(r);

		assert ServeTask.matchesRoute(r, "GET", "/some/1/<>");
		assert ServeTask.matchesRoute(r, "GET", "/some/<>/fancy");
		assert ServeTask.matchesRoute(r, "GET", "/<>/<>/fancy");
		assert !ServeTask.matchesRoute(r, "GET", "/some/<>");
		assert !ServeTask.matchesRoute(r, "GET", "/some/1");
		assert !ServeTask.matchesRoute(r, "GET", "/some");
	}

}
