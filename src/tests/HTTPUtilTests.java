
import java.io.StringReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

class
HTTPUtilTests {

	public static void
	main(String... args)
	throws IOException
	{
		responseToStringBasicFunctionality();
		requestReaderHaltsAtErrorSpots();
		requestReaderHaltsWithoutContentLength();
		acceptRequestLineUnpacksQueries();
		validHTTPRequestBasicFunctionality();
	}

	public static void
	responseToStringBasicFunctionality()
	throws IOException
	{
		HTTPResponse r = new HTTPResponse();
		r.method = "PUT";
		r.code = 200;
		String s1 = HTTPResponseWriter.toString(r);

		r.headers.put("Dish", "Toast");
		r.headers.put("Rating", "4");
		String s2 = HTTPResponseWriter.toString(r);

		r.body =
			"Toast is basic, but it does taste\n" +
			"pretty nice. Hence why I gave it\n" +
			"such a high rating.";
		String s3 = HTTPResponseWriter.toString(r);

		String sc1 = "HTTP/1.1 200\r\n\r\n";
		String sc2 =
			"HTTP/1.1 200\r\n" +
			"Dish: Toast\r\n" +
			"Rating: 4\r\n\r\n";
		String sc3 = sc2 + r.body;
		assert s1.equals(sc1);
		assert s2.equals(sc2);
		assert s3.equals(sc3);
	}

	public static void
	requestReaderHaltsAtErrorSpots()
	throws IOException
	{
		String s1 =
			"<html>This is not a HTTP request\r\n" +
			"even: if it contains header lines\r\n";
		HTTPRequest r1 = new HTTPRequest();
		HTTPRequestReader.read(r1, new StringReader(s1));

		String s2 =
			"GET /path HTTP/1.1\r\n" +
			"Header: Valid, despite bad name\r\n" +
			"Not a header\r\n\r\n";
		HTTPRequest r2 = new HTTPRequest();
		HTTPRequestReader.read(r2, new StringReader(s2));

		assert r1.method == null;
		assert r1.headers.size() == 0;

		assert r2.method.equals("GET");
		assert r2.headers.containsKey("Header");
		assert r2.headers.size() == 1;
		assert r2.body == null;
	}

	public static void
	requestReaderHaltsWithoutContentLength()
	throws IOException
	{
		String s1 =
			"GET /fancyUri?id=4 HTTP/1.1\r\n" +
			"Dish: French fries\r\n" +
			"Vegetable: Potato\r\n" +
			"\r\n" +
			"There's nothing wrong with this HTTP request. " +
			"Apart from that it doesn't set a Content-Length " +
			"header, therefore apparently, this body should " +
			"be ignored by the reader.";
		HTTPRequest r1 = new HTTPRequest();
		HTTPRequestReader.read(r1, new StringReader(s1));

		assert r1.uri.toString().equals("/fancyUri?id=4");
		assert r1.headers.size() == 2;
		assert r1.body == null;
	}

	public static void
	acceptRequestLineUnpacksQueries()
	throws IOException
	{
		String s1 = "PUT /drinks?name=Root%20Beer&category=Cola";
		HTTPRequest r = new HTTPRequest();
		HTTPRequestReader.acceptRequestLine(r, s1);

		String scq = "name=Root Beer&category=Cola";
		assert r.uri.getQuery().equals(scq);
		assert r.params.size() == 2;
		assert r.params.get("name").equals("Root Beer");
		assert r.params.get("category").equals("Cola");
	}

	public static void
	validHTTPRequestBasicFunctionality()
	throws IOException
	{
		HTTPRequest r = new HTTPRequest();
		assert !HTTPUtil.validHTTPRequest(r);

		r.method = "GET";
		String path = "/not_interesting";
		try { r.uri = new java.net.URI(path); }
		catch (URISyntaxException eUri) { assert false; }
		r.headers.put("Header", "Value");
		assert HTTPUtil.validHTTPRequest(r);

		r.method = " GET ";
		assert !HTTPUtil.validHTTPRequest(r);
		r.method = "get";
		assert !HTTPUtil.validHTTPRequest(r);
		r.method = "something funky";
		assert !HTTPUtil.validHTTPRequest(r);
		r.method = "PLAUSIBLYNEW";
		assert HTTPUtil.validHTTPRequest(r);

		r.headers.put(" ", "Value");
		assert !HTTPUtil.validHTTPRequest(r);
		r.headers.remove(" ");
		r.headers.put(null, "Value");
		assert !HTTPUtil.validHTTPRequest(r);
		r.headers.remove(null);
		r.headers.put("", "Value");
		assert !HTTPUtil.validHTTPRequest(r);
		r.headers.remove("");
		assert HTTPUtil.validHTTPRequest(r);

		r.body = null;
		assert HTTPUtil.validHTTPRequest(r);
		r.body = "";
		assert !HTTPUtil.validHTTPRequest(r);
		r.headers.put("Content-Length", "0");
		assert HTTPUtil.validHTTPRequest(r);
		r.body = "A line.\n";
		int n = r.body.getBytes().length;
		r.headers.put("Content-Length", "" + n);
		assert HTTPUtil.validHTTPRequest(r);
	} 

}
