
import java.sql.SQLException;
import java.io.IOException;
import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.JsonArray;
import javax.json.JsonException;
import javax.json.stream.JsonParsingException;
import java.util.Base64;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

class
ClientAPITests {

	public static void
	main(String... args)
	throws SQLException, IOException, ConfigAPI.ConfigException
	{
		ConfigAPI configAPI = new ConfigAPI();
		ConfigAPI.DbConnProfile dbConnProfile = configAPI
			.readDbConnProfile("dev");
		assert dbConnProfile != null;

		DataAPI dataAPI = new DataAPI(dbConnProfile);
		TestDataUtil.fillWithTestMainData(dataAPI);
		TestDataUtil.fillWithTestObjectData(dataAPI);
		NetworkAPI networkAPI = new NetworkAPI(dataAPI);
		JobAPI jobAPI = new JobAPI(dataAPI);
		Logger logger = new Logger(null);

		testRegisterRoute(dataAPI, logger);
		testAuthenticateRoute(dataAPI, logger);
		testCreateNoteRoute(dataAPI, networkAPI, jobAPI, logger);
		testReadInboxRoute(dataAPI, logger);
		testReadFederatedRoute(dataAPI, logger);

		jobAPI.shutdown();
	}

	public static void
	testRegisterRoute(DataAPI dataAPI, Logger logger)
	throws SQLException
	{
		RegisterRoute route = new RegisterRoute(dataAPI, logger);

		// Test blank request.
		HTTPRequest request = new HTTPRequest();
		HTTPResponse response = route.invoke(request);
		assert response.code == 400;

		// Test incomplete request.
		String username = "ranger" + (int)(Math.random() * 100);
		request.body = "{ \"username\": \"" + username + "\" }";
		response = route.invoke(request);
		assert response.code == 400;

		// Prepare valid request.
		String passphrase = "14passwordXYZ$500";
		request.body = "{ \"username\": \"" + username + "\", ";
		request.body += "\"passphrase\": \"" + passphrase + "\" }";

		// Later on, preload an existing account with
		// the same actor username and test that the
		// register endpoint doesn't somehow delete it.

		// Test valid response for valid request.
		response = route.invoke(request);
		assert response.code == 200;
		String ct = response.headers.get("Content-Type");
		assert ActivityPubUtil.contentTypeOk(ct);
		assert response.body != null;
		JsonObject rActor = DataAPI.toJsonObject(response.body);
		assert rActor != null;

		// Check that response exists within DB.
		String rActorId = rActor.getString("id");
		JsonObject dActor = dataAPI.getActor(rActorId);
		assert dActor != null;
		assert rActor.equals(Entities
			.filterPrivateFromActor(dActor));

		// Test that account was created for the username.
		// I think TestDataUtil already made these, though..
		DataAPI.Account dAccount = dataAPI.getAccount(username);
		assert dAccount != null;

		System.err.println("testRegisterRoute passed.");
	}

	public static void
	testAuthenticateRoute(DataAPI dataAPI, Logger logger)
	throws SQLException
	{
		AuthenticateRoute route = new AuthenticateRoute(
			dataAPI, logger);
		
		// Test blank request.
		HTTPRequest request = new HTTPRequest();
		HTTPResponse response = route.invoke(request);
		assert response.code == 400;

		// Check first that test entity is in DB.
		String tActorId = ActivityPubUtil
			.localShortIdToId("actor", "Elizabeth");
		assert dataAPI.getActor(tActorId) != null;

		// Test incomplete request.
		request.body = "{ \"username\": \"Elizabeth\" }";
		response = route.invoke(request);
		assert response.code == 400;

		// Test wrong passphrase.
		request.body = "{ \"username\": \"Elizabeth\", ";
		request.body += "\"passphrase\": \"wrongpassword\" }";
		response = route.invoke(request);
		assert response.code == 401;

		// Test right passphrase.
		request.body = "{ \"username\": \"Elizabeth\", ";
		request.body += "\"passphrase\": \"sambaloelek\" }";
		response = route.invoke(request);
		assert response.code == 200;
		assert response.body != null;

		// Check that access token is valid.
		try
		{
			int l = response.body.length() - "\r\n".length();
			String accessToken = response.body.substring(0, l);
			Base64.getDecoder().decode(accessToken);
		}
		catch (IllegalArgumentException eIa)
		{
			assert false;
		}

		System.err.println("testAuthenticateRoute passed.");
	}

	public static void
	testCreateNoteRoute(
		DataAPI dataAPI, NetworkAPI networkAPI,
		JobAPI jobAPI, Logger logger)
	throws SQLException
	{
		CreateNoteRoute route = new CreateNoteRoute(
			dataAPI, networkAPI, jobAPI, logger);

		// Load access token.
		HTTPRequest request = new HTTPRequest();
		String token = dataAPI.createAccessToken("Elizabeth");
		request.headers.put("Authorisation", "Bearer " + token);

		// Test blank request.
		HTTPResponse response = route.invoke(request);
		assert response.code == 400;

		// Prepare simple request.
		String content = "little fishy...";
		request.body = Json.createObjectBuilder()
			.add("content", content)
			.add("propagation", "Public")
			.add("targets", Json.createArrayBuilder().build())
			.build().toString();
		int n = request.body.getBytes().length;
		request.headers.put("Content-Length", "" + n);
		request.headers.put("Content-Type", "application/json");

		// Test valid response for simple request.
		response = route.invoke(request);
		assert response.code == 200;
		assert response.body != null;
		JsonObject rActivity = DataAPI.toJsonObject(response.body);
		assert rActivity != null;

		// Check that response exists in DB.
		String rActivityId = rActivity.getString("id");
		JsonObject dActivity = dataAPI.getActivity(rActivityId);
		assert dActivity != null;
		assert rActivity.equals(Entities
			.filterPrivateFromActivity(dActivity));

		// Check that object in response exists in DB, and that
		// its contents were what we submitted.
		String rObjectId = rActivity.getString("object");
		JsonObject dObject = dataAPI.getObject(rObjectId);
		assert dObject != null;
		assert dObject.getString("content").equals(content);

		// (未) Check dispatch into collection tables

		System.err.println("testCreateNoteRoute passed.");
	}

	public static void
	testReadInboxRoute(DataAPI dataAPI, Logger logger)
	throws SQLException
	{
		ReadInboxRoute route = new ReadInboxRoute(dataAPI, logger);

		// Load access token.
		HTTPRequest request = new HTTPRequest();
		String token = dataAPI.createAccessToken("sachiku17");
		request.headers.put("Authorisation", "Bearer " + token);

		// Test request without body.
		HTTPResponse response = route.invoke(request);
		assert response.code != 200;

		// Test request with default params.
		request.body = "{}";
		response = route.invoke(request);
		assert response.code == 200;
		JsonArray entities = toJsonArray(response.body);
		assert entities != null;

		// Detailed check on first entity returned.
		assert entities.size() > 0;
		assert entities.get(0) instanceof JsonObject;
		String id = ActivityPubUtil
			.localShortIdToId("activity", "66");
		String actorId = ActivityPubUtil
			.localShortIdToId("actor", "sachiku17");
		String objectId = ActivityPubUtil
			.localShortIdToId("object", "50");
		JsonObject entity = (JsonObject)entities.get(0);
		assert entity.getString("id").equals(id);
		assert entity.getString("actor").equals(actorId);
		assert entity.getString("object").equals(objectId);

		// Check entities overall.
		int defaultPageSize = 10;
		assert entities.size() == defaultPageSize;
		ZonedDateTime lastDate = ZonedDateTime.now();
		// Our test values are all in the past from the
		// time of writing, so this initial value is ok.
		for (JsonValue value: entities)
		{
			assert value instanceof JsonObject;
			entity = (JsonObject)value;

			assert entity.getString("actor").equals(actorId);

			ZonedDateTime date = getCreationDate(entity);
			assert date.isBefore(lastDate);

			lastDate = date;
		}

		// Test request with specific page size.
		request.body = "{ \"pageSize\": 5 }";
		response = route.invoke(request);
		assert response.code == 200;
		entities = toJsonArray(response.body);
		assert entities != null;
		assert entities.size() == 5;

		// Test flipping page.
		ZonedDateTime firstPageOldest = getCreationDate(
			(JsonObject)entities.get(entities.size() - 1));
		request.body = "{" +
			"\n \"pageSize\": 5" + "," +
			"\n \"before\": \"" + firstPageOldest + "\" }";
		response = route.invoke(request);
		assert response.code == 200;
		entities = toJsonArray(response.body);
		ZonedDateTime secondPageNewest = getCreationDate(
			(JsonObject)entities.get(0));
		assert secondPageNewest.isBefore(firstPageOldest);

		System.err.println("testReadInboxRoute passed.");
	}

	public static void
	testReadFederatedRoute(DataAPI dataAPI, Logger logger)
	throws SQLException
	{
		ReadFederatedRoute route = new ReadFederatedRoute(
			dataAPI, logger);

		HTTPRequest request = new HTTPRequest();
		HTTPResponse response = route.invoke(request);
		assert response.code == 200;
		JsonArray entities = toJsonArray(response.body);
		assert entities != null;

		assert entities.size() > 0;
		for (JsonValue value: entities)
		{
			assert value instanceof JsonObject;
		}

		System.err.println("testReadFederatedRoute passed.");
	}

//	 -	-%-	 -

	private static JsonArray
	toJsonArray(String jsonr)
	{
		try
		{
			StringReader s = new StringReader(jsonr);
			return Json.createReader(s).readArray();
		}
		catch (JsonParsingException eJp)
		{
			assert false;
			return null;
		}
		catch (JsonException eJ)
		{
			assert false;
			return null;
		}
	}

	private static ZonedDateTime
	getCreationDate(JsonObject entity)
	{
		assert entity != null;

		String dater = entity.getString("created", null);
		assert dater != null;

		try
		{
			return ZonedDateTime.parse(dater);
		}
		catch (DateTimeParseException eDtp)
		{
			assert false;
			return null;
		}
	}

}
