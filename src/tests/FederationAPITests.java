
import java.sql.SQLException;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.JsonArray;

class
FederationAPITests {

	public static void
	main(String... args)
	throws SQLException, IOException, ConfigAPI.ConfigException
	{
		ConfigAPI configAPI = new ConfigAPI();
		ConfigAPI.DbConnProfile dbConnProfile = configAPI
			.readDbConnProfile("dev");
		assert dbConnProfile != null;

		DataAPI dataAPI = new DataAPI(dbConnProfile);
		TestDataUtil.fillWithTestMainData(dataAPI);
		TestDataUtil.fillWithTestObjectData(dataAPI);
		Logger logger = new Logger(null);

		testWebfingerRoute(dataAPI, logger);
		testGetLocalActorRoute(dataAPI, logger);
		testGetLocalObjectRoute(dataAPI, logger);
		testGetLocalActivityRoute(dataAPI, logger);
	}

	public static void
	testWebfingerRoute(DataAPI dataAPI, Logger logger)
	throws SQLException
	{
		WebfingerRoute route = new WebfingerRoute(
			dataAPI, logger);

		// Test blank request.
		HTTPRequest request = new HTTPRequest();
		HTTPResponse response = route.invoke(request);
		assert response.code == 400;

		// Prepare valid request.
		final String HOST = "localhost";
		String username = "WeeKiong";
		String subject = "acct:" + username + "@" + HOST;
		request.params.put("resource", subject);

		// Test valid headers for valid request.
		response = route.invoke(request);
		assert response.code == 200;
		String ct = response.headers.get("Content-Type");
		assert ActivityPubUtil.contentTypeOk(ct);
		// Okay in what sense...?

		// Test valid body for valid request.
		assert response.body != null;
		JsonObject json = DataAPI.toJsonObject(response.body);
		assert json != null;
		assert json.getString("subject", "").startsWith("acct");
		assert json.get("links") instanceof JsonArray;

		boolean hasSelfLink = false;
		// A Webfinger response gives an array of links
		// for the resource. Test that it has the self link
		// necessary for ActivityPub.
		final String T = "application/activity+json";
		String prefix = "https://" + HOST + "/actors/";
		JsonArray links = json.getJsonArray("links");
		assert !links.isEmpty();
		for (JsonValue link: links)
		{
			if (!(link instanceof JsonObject)) continue;
			JsonObject json2 = (JsonObject)link;
			boolean m = true;
			m &= json2.getString("rel", "").equals("self");
			m &= json2.getString("type", "").equals(T);
			m &= json2.getString("href", "").startsWith(prefix);
			if (m) hasSelfLink = true;
		}
		assert hasSelfLink;

		System.err.println("testWebfingerRoute passed.");
	}

	public static void
	testGetLocalActorRoute(DataAPI dataAPI, Logger logger)
	throws SQLException
	{
		GetLocalActorRoute route = new GetLocalActorRoute(
			dataAPI, logger);

		// Test random request.
		HTTPRequest request = new HTTPRequest();
		request.params.put("\b1", "" + Short.MAX_VALUE);
		HTTPResponse response = route.invoke(request);
		assert response.code != 200;

		// Test valid request.
		request.params.put("\b1", "Ming_Jia");
		response = route.invoke(request);
		assert response.code == 200;
		String ct = response.headers.get("Content-Type");
		assert ActivityPubUtil.contentTypeOk(ct);
		assert response.body != null;
		JsonObject json = DataAPI.toJsonObject(response.body);
		assert json != null;

		System.err.println("testGetLocalActorRoute passed.");
	}

	public static void
	testGetLocalObjectRoute(DataAPI dataAPI, Logger logger)
	throws SQLException
	{
		GetLocalObjectRoute route = new GetLocalObjectRoute(
			dataAPI, logger);

		// Test random request.
		HTTPRequest request = new HTTPRequest();
		request.params.put("\b1", "" + Short.MAX_VALUE);
		HTTPResponse response = route.invoke(request);
		assert response.code != 200;

		// Test valid request.
		request.params.put("\b1", "10");
		response = route.invoke(request);
		assert response.code == 200;
		String ct = response.headers.get("Content-Type");
		assert ActivityPubUtil.contentTypeOk(ct);
		assert response.body != null;
		JsonObject json = DataAPI.toJsonObject(response.body);
		assert json != null;

		System.err.println("testGetLocalObjectRoute passed.");
	}

	public static void
	testGetLocalActivityRoute(DataAPI dataAPI, Logger logger)
	throws SQLException
	{
		GetLocalActivityRoute route = new GetLocalActivityRoute(
			dataAPI, logger);

		// Test random request.
		HTTPRequest request = new HTTPRequest();
		request.params.put("\b1", "" + Short.MAX_VALUE);
		HTTPResponse response = route.invoke(request);
 		assert response.code != 200;

		// Test valid request.
		request.params.put("\b1", "1");
		response = route.invoke(request);
		assert response.code == 200;
		String ct = response.headers.get("Content-Type");
		assert ActivityPubUtil.contentTypeOk(ct);
		assert response.body != null;
		JsonObject json = DataAPI.toJsonObject(response.body);
		assert json != null;

		System.err.println("testGetLocalActivityRoute passed.");
	}

}
