
import java.io.Writer;
import java.io.PrintWriter;
import java.time.LocalDateTime;

class
Logger {

	private PrintWriter
	w;

//	---%-@-%---

	public void
	log(String message)
	{
		if (w == null) return;
		LocalDateTime time = LocalDateTime.now();

		w.print("[" + time + "] ");
		w.println(message);
		w.flush();
	}

	public void
	log(String[] messages, String prefix)
	{
		if (w == null) return;

		for (String message: messages) log(prefix + message);
	}

	public void
	log(Throwable e)
	{
		if (w == null) return;

		log("", e);
	}

	public void
	log(String msg, Throwable e)
	{
		if (w == null) return;
		LocalDateTime time = LocalDateTime.now();

		w.print("[" + time + "] " + msg);
		e.printStackTrace(w);
		w.flush();
	}

//	---%-@-%---

	Logger()
	{
		this(new PrintWriter(System.err));
	}

	Logger(PrintWriter w)
	{
		this.w = w;
	}

}