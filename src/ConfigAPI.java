
import java.io.FileReader;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonArray;
import javax.json.JsonValue;
import javax.json.JsonString;
import javax.json.JsonException;
import javax.json.stream.JsonParsingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

class
ConfigAPI {

	public DbConnProfile
	readDbConnProfile(String name)
	throws ConfigException
	{
		return readDbConnProfiles().get(name);
	}

	public Map<String, DbConnProfile>
	readDbConnProfiles()
	throws ConfigException
	{
		String configPath = "config.json";

		try
		{
			FileReader r = new FileReader(configPath);
			/*
			* Don't use the classloader for now, it seems it's
			* reading from within bin which is annoying to
			* copy assets to and makes our db folder invalid.
			* Stick with files from the execution CWD.
			*/
			JsonObject config = Json.createReader(r).readObject();

			return parseDbConnProfiles(config.getJsonObject("db"));
		}
		catch (IOException | JsonException eIo)
		{
			throw new ConfigException(null, eIo);
		}
	}

//	 -	-%-	 -

	private static Map<String, DbConnProfile>
	parseDbConnProfiles(JsonObject dbConnProfiles)
	{
		Map<String, DbConnProfile> returnee = new HashMap<>();
		for (String name: dbConnProfiles.keySet())
		{
			JsonObject dbConnProfile = dbConnProfiles
				.getJsonObject(name);
			returnee.put(name, parseDbConnProfile(dbConnProfile));
		}
		return returnee;
	}

	private static DbConnProfile
	parseDbConnProfile(JsonObject dbConnProfile)
	{
		DbConnProfile returnee = new DbConnProfile();
		for (String key: dbConnProfile.keySet())
		{
			JsonValue value = dbConnProfile.get(key);
			considerString(returnee, key, value);
			considerStringArray(returnee, key, value);
			considerBoolean(returnee, key, value);
		}
		return returnee;
	}

	private static void
	considerString(
		DbConnProfile profile, String key, JsonValue value)
	{
		// Check if value is a string.
		if (!(value instanceof JsonString)) return;

		// Convert to standard string.
		String v = ((JsonString)value).getString();

		// Populate.
		if (key.equals("specPrefix"))
			profile.specPrefix = v;

		if (key.equals("username"))
			profile.username = v;

		if (key.equals("passphrase"))
			profile.passphrase = v;
	}

	private static void
	considerStringArray(
		DbConnProfile profile, String key, JsonValue value)
	{
		// Check if value is a string array.
		if (!(value instanceof JsonArray)) return;
		JsonArray array = (JsonArray)value;
		for (JsonValue element: array)
			if (!(element instanceof JsonString)) return;

		// Convert to standard string array.
		String[] v = new String[array.size()];
		for (int o = 0; o < v.length; ++o)
			v[o] = array.getString(o);

		// Populate.
		if (key.equals("mainInitScripts"))
			profile.mainInitScripts = v;
		
		if (key.equals("objectInitScripts"))
			profile.objectInitScripts = v;
	}

	private static void
	considerBoolean(
		DbConnProfile profile, String key, JsonValue value)
	{
		// Check if value is a boolean.
		boolean typeOk = false;
		typeOk |= value == JsonValue.TRUE;
		typeOk |= value == JsonValue.FALSE;
		if (!typeOk) return;

		// Convert to standard boolean.
		boolean v = value == JsonValue.TRUE;

		// Populate.
		if (key.equals("ignoreDatabaseVersion"))
			profile.ignoreDatabaseVersion = v;
	}

//	---%-@-%---

	static class
	ConfigException extends Exception {

		ConfigException(String error, Throwable cause)
		{
			super(error, cause);
		}

	}

	static class
	DbConnProfile {

		String
		specPrefix;

		String
		username, passphrase;

		String[]
		mainInitScripts = new String[0],
		objectInitScripts = new String[0];

		boolean
		ignoreDatabaseVersion;

	}

}