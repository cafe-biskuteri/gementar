
import java.sql.SQLException;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonException;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.stream.JsonParsingException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

class
RegisterRoute implements Route {

	DataAPI
	dataAPI;

	Logger
	logger;

//	---%-@-%---

	public String
	getMethod() { return "POST"; }

	public String
	getPath() { return "/register"; }

	public GemetarRouteRequirements
	getRouteRequirements()
	{
		GemetarRouteRequirements returnee =
			new GemetarRouteRequirements();

		Map<String, Class> bp = new HashMap<>();
		bp.put("username", JsonString.class);
		bp.put("passphrase", JsonString.class);
		returnee.requiredBodyParams = bp;

		return returnee;
	}

	public HTTPResponse
	invoke(HTTPRequest request)
	throws SQLException
	{
		HTTPResponse response = new HTTPResponse();
		GemetarRouteRequirements req = getRouteRequirements();
		boolean ok = Gemetar
			.enforceRouteRequirements(req, request, response);
		if (!ok) return response;


		String username = req.bodyParams.getString("username");
		JsonObject newActorEntity = Entities
			.generateActor(Json.createObjectBuilder()
				.add("username", username)
				.build());

		String passphrase = req.bodyParams.getString("passphrase");
		dataAPI.saveActor(newActorEntity);
		dataAPI.createAccount(username, passphrase);
		// (未) These should be as one transaction.


		newActorEntity = Entities
			.filterPrivateFromActor(newActorEntity);

		response.method = request.method;
		response.uri = request.uri;
		response.code = 200;
		response.headers.put("Content-Type", "application/json");
		response.body = newActorEntity.toString();
		// Is this really useful? Shouldn't we give a new access
		// token instead, or a body that has both an access
		// token and this new actor entity.
		return response;
	}

//	---%-@-%---

	RegisterRoute(DataAPI dataAPI, Logger logger)
	{
		this.dataAPI = dataAPI;
		this.logger = logger;
	}

}


class
AuthenticateRoute implements Route {

	DataAPI
	dataAPI;

	Logger
	logger;

//	---%-@-%---

	public String
	getMethod() { return "GET"; }

	public String
	getPath() { return "/auth"; }

	public GemetarRouteRequirements
	getRouteRequirements()
	{
		GemetarRouteRequirements returnee =
			new GemetarRouteRequirements();

		Map<String, Class> bp = new HashMap<>();
		bp.put("username", JsonString.class);
		bp.put("passphrase", JsonString.class);
		returnee.requiredBodyParams = bp;

		return returnee;
	}

	public HTTPResponse
	invoke(HTTPRequest request)
	throws SQLException
	{
		HTTPResponse response = new HTTPResponse();
		GemetarRouteRequirements req = getRouteRequirements();
		boolean ok = Gemetar
			.enforceRouteRequirements(req, request, response);
		if (!ok) return response;


		String username = req.bodyParams.getString("username");
		String passphrase = req.bodyParams.getString("passphrase");
		if (!dataAPI.passphraseMatches(username, passphrase))
		{
			response.code = 401;
			response.body = "Passphrase doesn't match.";
			return response;
		}
		/*
		* By the by, shouldn't we have the client do the hashing,
		* and we straight compare with the hash on file or we
		* hash the salted given hash for a second-level hash?
		*/

		String token = dataAPI.createAccessToken(username);
		if (token == null)
		{
			response.code = 500;
			return response;
		}


		response.method = request.method;
		response.uri = request.uri;
		response.code = 200;
		response.body = token;
		return response;
	}

//	---%-@-%---

	AuthenticateRoute(DataAPI dataAPI, Logger logger)
	{
		this.dataAPI = dataAPI;
		this.logger = logger;
	}

}


class
CreateNoteRoute implements Route {

	DataAPI
	dataAPI;

	NetworkAPI
	networkAPI;

	JobAPI
	jobAPI;

	Logger
	logger;

//	---%-@-%---

	public String
	getMethod() { return "POST"; }

	public String
	getPath() { return "/create/note"; }

	public GemetarRouteRequirements
	getRouteRequirements()
	{
		GemetarRouteRequirements returnee =
			new GemetarRouteRequirements();

		Map<String, Class> bp = new HashMap<>();
		bp.put("content", JsonString.class);
		bp.put("propagation", JsonString.class);
		bp.put("targets", JsonArray.class);
		returnee.requiredBodyParams = bp;

		returnee.authRequired = true;
		returnee.dataAPI = this.dataAPI;

		return returnee;
	}

	public HTTPResponse
	invoke(HTTPRequest request)
	throws SQLException
	{
		HTTPResponse response = new HTTPResponse();
		GemetarRouteRequirements req = getRouteRequirements();
		boolean ok = Gemetar
			.enforceRouteRequirements(req, request, response);
		if (!ok) return response;

		JsonObject createdNote = Entities
			.generateObject(Json.createObjectBuilder()
				.add("shortId", dataAPI.generateObjectShortId())
				.add("type", "Note")
				.add("content", req.bodyParams.getString("content"))
				.add("created", ZonedDateTime.now().toString())
				.build());

		JsonObject createdActivity = Entities
			.generateActivity(Json.createObjectBuilder()
				.add("shortId", dataAPI.generateActivityShortId())
				.add("type", "Create")
				.add("actor", req.userEntity.getString("id"))
				.add("object", createdNote.getString("id"))
				.add("propagation", req.bodyParams
					.getJsonString("propagation"))
				.add("targets", req.bodyParams
					.getJsonArray("targets"))
				.build());

		dataAPI.saveObject(createdNote);
		dataAPI.saveActivity(createdActivity);
		// (未) As one transaction.
		// Do we have any plans for idempotency in DataAPI?

		String creatorUsername = req.userEntity
			.getString("username");
		dataAPI.saveInOutbox(creatorUsername, createdActivity);
		// And, will arrive in our inbox per delivery job.

		Callable<Void> deliveryJob = new DeliverJob(
			dataAPI, networkAPI,
			createdActivity.getString("id"));
		jobAPI.queue(deliveryJob, deliveryJob.toString());

		response.method = request.method;
		response.uri = request.uri;
		response.code = 200;
		response.headers.put("Content-Type", "application/json");
		response.body = Entities
			.filterPrivateFromActivity(createdActivity)
			.toString();
		return response;
	}

//	---%-@-%---

	CreateNoteRoute(
		DataAPI dataAPI, NetworkAPI networkAPI,
		JobAPI jobAPI, Logger logger)
	{
		this.dataAPI = dataAPI;
		this.networkAPI = networkAPI;
		this.jobAPI = jobAPI;
		this.logger = logger;
	}

}



class
ReadInboxRoute implements Route {

	DataAPI
	dataAPI;

	Logger
	logger;

//	---%-@-%---

	public String
	getMethod() { return "GET"; }

	public String
	getPath() { return "/actors/<>/inbox"; }

	public GemetarRouteRequirements
	getRouteRequirements()
	{
		GemetarRouteRequirements returnee =
			new GemetarRouteRequirements();

		returnee.authRequired = true;
		returnee.dataAPI = this.dataAPI;

		returnee.requiredBodyParams = new HashMap<>();

		return returnee;
	}

	public HTTPResponse
	invoke(HTTPRequest request)
	throws SQLException
	{
		HTTPResponse response = new HTTPResponse();
		GemetarRouteRequirements req = getRouteRequirements();
		boolean ok = Gemetar
			.enforceRouteRequirements(req, request, response);
		if (!ok) return response;

		Parameters params = prepareOptionalParameters(req);
		if (!params.error.isEmpty())
		{
			response.code = 400;
			response.body = params.error;
			return response;
		}

		String username = req.userEntity.getString("username");
		assert username != null;

		// Probably should instead bark if username does not
		// match the route path's owner. Rather than mysteriously
		// just give the user's own.

		List<String> activityIds = dataAPI.readInbox(
			username,
			params.before, params.after,
			params.pageSize);

		JsonArrayBuilder b = Json.createArrayBuilder();
		for (String id: activityIds)
		{
			JsonObject activity = dataAPI.getActivity(id);
			if (activity == null) continue;
			b.add(Entities.filterPrivateFromActivity(activity));
		}

		response.method = request.method;
		response.uri = request.uri;
		response.code = 200;
		response.headers.put("Content-Type", "application/json");
		response.body = b.build().toString();
		return response;
	}

//	 -	-%-	 -

	private Parameters
	prepareOptionalParameters(GemetarRouteRequirements req)
	{
		Parameters returnee = new Parameters();

		if (req.bodyParams == null) return returnee;
	
		if (req.bodyParams.containsKey("before"))
		{
			returnee.before = parseZonedDateTime(
				req.bodyParams.getString("before", null));

			if (returnee.before == null)
				returnee.error +=
					"'before' is not a valid timespec.\n";
		}

		if (req.bodyParams.containsKey("after"))
		{
			returnee.after = parseZonedDateTime(
				req.bodyParams.getString("after", null));

			if (returnee.after == null)
				returnee.error +=
					"'after' is not a valid timespec.\n";
		}

		if (req.bodyParams.containsKey("pageSize"))
		{
			returnee.pageSize =
				req.bodyParams.getInt("pageSize", 0);

			if (returnee.pageSize <= 0)
				returnee.error +=
					"'pageSize' is not a valid positive number.\n";
		}

		return returnee;
	}

	private ZonedDateTime
	parseZonedDateTime(String dater)
	{
		if (dater == null) { return null; }
		else try { return ZonedDateTime.parse(dater); }
		catch (DateTimeParseException eDtp) { return null; }
	}

//	---%-@-%---

	class
	Parameters {

		ZonedDateTime
		before = null,
		after = null;

		int
		pageSize = 10;

		String
		error = "";

	}

//	---%-@-%---

	ReadInboxRoute(DataAPI dataAPI, Logger logger)
	{
		this.dataAPI = dataAPI;
		this.logger = logger;
	}

}



class
ReadFederatedRoute implements Route {

	DataAPI
	dataAPI;

	Logger
	logger;

//	---%-@-%---

	public String
	getMethod() { return "GET"; }

	public String
	getPath() { return "/federated"; }

	public HTTPResponse
	invoke(HTTPRequest request)
	throws SQLException
	{
		List<JsonObject> activities = dataAPI
			.readActivities(null, null, 8);
		JsonArrayBuilder b = Json.createArrayBuilder();
		for (JsonObject activity: activities)
			b.add(Entities.filterPrivateFromActivity(activity));

		HTTPResponse response = new HTTPResponse();
		response.method = request.method;
		response.uri = request.uri;
		response.code = 200;
		response.headers.put("Content-Type", "application/json");
		response.body = b.build().toString();
		return response;
	}

//	---%-@-%---

	ReadFederatedRoute(DataAPI dataAPI, Logger logger)
	{
		this.dataAPI = dataAPI;
		this.logger = logger;
	}

}
